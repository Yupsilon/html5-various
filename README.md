# Battlecry #

A sideways RTS game where you controll your 3 heroes, an orc, a archer and a thief, and must defeat more characters than your rival. Developed in Javascript for HTML5.

# Jumpgame #

Typescript mobile application developed in typescript. Uses the PIXI and THREE libraries to render the UI and world.

# Installation #

Only a text editor is needed. You can run the game's '.html' in any browser to play the game.