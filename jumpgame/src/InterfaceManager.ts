/*     -----  INTERFACE MANAGER -----

Class responsible for various interface elements and canvas
Values:
 - uicanvas2D: the canvas that displays the UI
 - GameOverText: the GAME OVER text that appears when the player loses
Functions:
 - init_UI_canvas: Initializes the UI interface
 - DrawGameOverText: draws the GAME OVER text that will show when the player is killed
 - HideGameOverText: Hides/unhides the GAME OVER text
*/

import * as PIXI from 'pixi.js';

export default class InterfaceManager
{
    uicanvas2D : PIXI.Application;
    GameOverText : PIXI.Text;

    constructor()
    {
        this.init_UI_canvas();
        this.DrawGameOverText();
    }


    init_UI_canvas():void
    {
        this.uicanvas2D = new PIXI.Application({
            width : globalThis.Game.width,
            height : globalThis.Game.height,
            backgroundAlpha: 0,
        });
        
        document.body.appendChild(this.uicanvas2D.view);
        this.uicanvas2D.view.style.position = "absolute";
    }

    DrawGameOverText() : void
    {

        this.GameOverText = new PIXI.Text();
        this.GameOverText.position.x = globalThis.Game.width/6;
        this.GameOverText.position.y = globalThis.Game.height / 2;

        this.GameOverText.text = "GAME OVER!";
        this.GameOverText.style.fontSize = globalThis.Game.width/10;       
        this.GameOverText.style.fill = 0xAA0000; 
        this.GameOverText.style.stroke = 0x662222;
        this.GameOverText.style.strokeThickness = globalThis.Game.width/1000;

        this.uicanvas2D.stage.addChild(this.GameOverText);
    }
    HideGameOverText(value:boolean)
    {
        this.GameOverText.alpha = value ? 0 : 1;
    }
}