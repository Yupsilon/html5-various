/*     -----  BACKGROUND CONTROLLER -----

Class responsible for keeping track of player score and updating interface
Values:
 - bgSprite: the static background sprite
 - buiSprite: the paralax looping sprite used for the tower
Functions:
 - DrawBackground: Initializes the PIXI elements for the background
 - UpdateVisuals: Changes the position of the tower sprite
*/

import * as PIXI from 'pixi.js';

import bg from '../resource/2D/Vector-Mountain-Moon-Sky-Phone-Wallpaper.jpg'
import bui from '../resource/2D/BGbuilding.png'

export default class BackgroundManager {
    bgSprite : PIXI.Sprite;
    buiSprite : PIXI.TilingSprite;
    constructor()
    {
        this.DrawBackground()
    }
    DrawBackground():void {

        this.bgSprite  = new PIXI.Sprite( PIXI.Texture.from(bg));
        globalThis.Game.gamecanvas2D.stage.addChild(this.bgSprite);
        this.bgSprite.scale.x = globalThis.Game.width / 1080;
        this.bgSprite.scale.y = globalThis.Game.height / 2000;

        this.buiSprite  = new PIXI.TilingSprite( PIXI.Texture.from(bui), 541, globalThis.Game.height);
        globalThis.Game.gamecanvas2D.stage.addChild(this.buiSprite);
        //buiSprite.scale.set(3,3);
        this.buiSprite.position.x = (globalThis.Game.height-541) / 2;
        
        let pixiTicker = PIXI.Ticker.shared;
        pixiTicker.add(()=>{this.UpdateVisuals()});
    }
    UpdateVisuals():void
    {
        if (this.buiSprite){
            this.buiSprite.tilePosition.y = globalThis.Game.worldHeight % 920;
        }
    }
}