/*     -----  SCORE CONTROLLER -----

Class responsible for keeping track of player score and updating interface
Values:
 - score: the current score of the player
 - highscore: the highest score the player obtained this run
Functions:
 - InitElements: Initializes the UI elements
 - IncreaseScore: increases the score by value, and updates the UI
 - Reset: Resets the score to 0, and updates the UI
 - UpdateScore: Updates the score values on the UI display
*/

import * as PIXI from 'pixi.js';

export default class ScoreController
{
    score : number = 0;
    high_score : number = 0;

    score_element : PIXI.Text;
    highscore_element : PIXI.Text;

    public constructor()
    {
        this.InitElements();
    }

    InitElements()
    {
        let text_style = new PIXI.TextStyle();
        text_style.align = 'center';
        text_style.fill = "white";
        text_style.stroke = 'black';
        text_style.strokeThickness = 3;
        text_style.fontSize = globalThis.Game.width/15;

        let score_text = new PIXI.Text();
        score_text.position.x = 25;
        score_text.position.y = 25;
        score_text.style = text_style;

        globalThis.Game.interface.uicanvas2D.stage.addChild(score_text);
        this.score_element = score_text;

        let highscore_text = new PIXI.Text();
        highscore_text.position.x = 25;
        highscore_text.position.y = 90;
        highscore_text.style = text_style;

        globalThis.Game.interface.uicanvas2D.stage.addChild(highscore_text);
        this.highscore_element = highscore_text;
    }
    public IncreaseScore(value:number)
    {
        this.score += value;
        this.high_score = Math.max(this.score,this.high_score);
        this.UpdateScore();
    }
    public Reset()
    {
        this.score = 0;
        this.UpdateScore();
    }
    UpdateScore()
    {
        if (this.score_element)
        {
            this.score_element.text = "Score: "+this.score;
        }
        if (this.highscore_element)
        {
            this.highscore_element.text = "High Score: "+this.high_score;
        }
    }
}