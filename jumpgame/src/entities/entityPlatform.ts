/*     -----  PLATFORM ENTITY -----

Class that handles the movement and visuals of platforms. Inherits entity base.
Values:
 - moveDirection: the current direction the platform is moving
 - moveSpeed: the speed of this platform
 - mySprite: the cloud sprite representing this platform
Functions:
 - InitSprite: Initializes the PIXI objects for this platform
 - Move: Moves this platform in the direction it is supposed to move
 - UpdateVisual: Updates the sprite position for this platform
 - Update: Calls Move
 - SetPosition: Changes the position of this platform, and updates the relative position of this sprite
*/

import * as PIXI from 'pixi.js';
import entityBase from "./entityBase";

import platformTexture from '../../resource/2D/Cloud.png'

enum Direction{
    left = -1,
    right = 1,
    stopped = 0
}

export default class entityPlatform extends entityBase
{
    width : number = 289;
    height : number = 93;

    moveDirection : Direction = 0;
    moveSpeed : number = 20;

    mySprite : PIXI.Sprite;

    public constructor()
    {
        super();
        this.InitSprite();
    }
    InitSprite()
    {
        let myTexture = PIXI.Texture.from(platformTexture)
        this.mySprite  = new PIXI.Sprite(myTexture );
        globalThis.Game.gamecanvas2D.stage.addChild(this.mySprite);
        this.SetScale(globalThis.Game.width / 900)
        this.moveSpeed = globalThis.Game.width / 60

        this.UpdateVisual();
        let ticker = PIXI.Ticker.shared;
        ticker.add(()=>{this.UpdateVisual();});
    }
    override SetScale(scale:number):void
    {
        super.SetScale(scale);
        this.mySprite.scale.set(scale,scale)   
    }
    override Move():void
    {
        if (this.moveDirection!=Direction.stopped)
        {
            if (this.moveDirection == Direction.right)
            {
                this.x += this.moveSpeed * globalThis.Game.difficulty;
                if (this.x > globalThis.Game.width / 2 - this.width / 2)
                {
                    this.x = globalThis.Game.width  / 2 - this.width / 2;
                    this.moveDirection = Direction.left;
                }
            }
            else 
            {
                this.x -= this.moveSpeed * globalThis.Game.difficulty;
                if (this.x < - globalThis.Game.width  / 2 + this.width / 2)
                {
                    this.x = - globalThis.Game.width  / 2 + this.width / 2;
                    this.moveDirection = Direction.right;
                }
            }
        }
        this.SetPosition(this.x, this.y);
    }
    public override UpdateVisual():void
    {
        let worldBottom = globalThis.Game.height + globalThis.Game.worldHeight

        this.mySprite.position.set(
            this.real_x - this.width / 2,
            this.real_y - this.height + worldBottom
            );
    }
    public override Update():void
    {
        this.Move();
    }
    public override SetPosition(nX,nY):void
    {
        super.SetPosition(nX,nY);
        this.real_x = globalThis.Game.width  / 2 + this.x;
        this.real_y = this.y * globalThis.Game.platformHeight;
    }
}
