/*     -----  PLAYER ENTITY -----

Player entity class, that updates the position and state of the player
Values:
 - currentState: the current state of the player; idle, charging, jumping, falling or dead
 - vel_x, vel_y: the current velocity of the player
 - vel_dir: the direction of the jump the player is aiming
 - jump_height: the height of the palyer
 - mySprite: the player sprite PIXI object, used for debug
 - numCircles: the number of circles that will be desplayed when the player is aiming
 - distCircles: the distance between the aiming circles
 - displayCircles: a list of PIXI Graphic elements representing the circle aimer
 - CharacterModel: the 3D model of the player
 - mixer: the animationmixer
 - animations: a list of animations for the player model
Functions:
 - initSprite: Creates a sprite for the player
 - SetScale: Sets the scale of the display objects, as well as width/height of the palyer
 - initJumpIndicator: Creates numCircles circles
 - HideJumpIndocators: Hides all the circles in the displayCircles array
 - UpdateJumpIndicator: Updates the position of all Jump Indicator to represent the player's jump direction
 - InitControlls: Initializes the player control
 - OnMouseDown, OnMouseUp: Handles palyer input, and changes player state accordingly
 - Restart: Restarts the position of the palyer, at the start of the game
 - ChangeState: Handles the player changing states
 - HandleState: Handles player behavior in the current state
 - AimJump: Updates the jump direction of the player, called in its charge state
 - Move: Moves the player in its jump and fall state
 - LandOnPlatform: Snaps the position of the player to a platform
 - IsOnPlatform: Checks if the player is on the current moving platform
 - SetPosition: Updates the position of the palyer, and the position of the model
 - UpdateVisual: Updates the sprite and 3D model of the player
 - Load3DModel: Loads the player's 3D model
 - PlayAnimation: Plays a specific animation
*/

import * as PIXI from 'pixi.js';
import entityPlatform from './entityPlatform'
import entityBase from "./entityBase";
import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import {lerp} from '../utils'

enum PlayerState{
    idle,
    charge,
    jump,
    fall,
    dead
}

enum PlayerAnimation{
    jump = 0,
    juggle = 1,
    ready = 2,
    hit = 3,
    idle = 4
}

export default class entityPlayer extends entityBase
{

    width : number = 100;
    height : number = 100;
    
    currentState : PlayerState;

    vel_x : number = 0;
    vel_y : number = 0;
    vel_dir : number = 1;
    
    jump_height : number = 100;
    
    mySprite : PIXI.Graphics;

    CharacterModel : THREE.Object3D;
    animations;
    mixer : THREE.AnimationMixer;

    numCircles : number = 8;
    distCircles : number = .025;
    displayCircles;

    public constructor()
    {
        super();
        this.jump_height = globalThis.Game.height * 1.33;

        this.initSprite()
        this.Load3DModel();
        this.initJumpIndicator();
        this.SetScale(globalThis.Game.width / 750)

        this.InitControlls();
        this.UpdateVisual();
        let ticker = PIXI.Ticker.shared;
        ticker.add(()=>{this.UpdateVisual();});
    }
    initSprite()
    {
        this.mySprite = new PIXI.Graphics();
        globalThis.Game.gamecanvas2D.stage.addChild(this.mySprite);
        this.mySprite.alpha = 0;
        this.mySprite.beginFill(0xFFFFFF);
        this.mySprite.drawCircle(0,0,this.height / 2);
        this.mySprite.endFill();
    }

    SetScale(scale:number):void
    {
        super.SetScale(scale);
        if (this.mySprite){
            this.mySprite.scale.set(scale,scale)
        }
        //this.CharacterModel.scale.set(scale,scale,scale)     
    }
    initJumpIndicator()
    {
        this.displayCircles = {};
        for (var i = 0; i<this.numCircles; i++)
        {
            let circle = new PIXI.Graphics();
            circle.beginFill(0x11FF11);
            circle.drawCircle(0,0,20-i*2);
            circle.endFill();
            globalThis.Game.gamecanvas2D.stage.addChild(circle);
            this.displayCircles[i] = circle;
        }
    }
    HideJumpIndocators(value:boolean)
    {
        for (var i = 0; i<this.numCircles; i++)
        {
            this.displayCircles[i].alpha = value ? 0 : 1;
        }
        if (value == true)
        {
            this.UpdateJumpIndicator();
        }
    }
    UpdateJumpIndicator()
    {
        let approx_vel_x = this.vel_x;

        let worldBottom = globalThis.Game.height + globalThis.Game.worldHeight
        let posX = this.real_x + approx_vel_x * this.distCircles;
        let posY = worldBottom + this.real_y + this.vel_y * this.distCircles - this.height;
        for (var i = 0; i<this.numCircles; i++)
        {
            this.displayCircles[i].position.set(posX,posY);
            posX += approx_vel_x * this.distCircles;
            posY += this.vel_y * this.distCircles;
            
            if (approx_vel_x > 0)
            {
                let borderX = globalThis.Game.width - this.width / 2
                if (posX > borderX)
                {
                    let velDelta = posX - borderX
                    posX = borderX - velDelta;

                    approx_vel_x*=-.5;
                }
            }
            else 
            {
                let borderX = this.width / 2
                if (posX < borderX)
                {
                    let velDelta = posX - borderX
                    posX = borderX - velDelta;

                    approx_vel_x*=-.5;
                }
            }
        }
    }
    public InitControlls()
    {
        window.addEventListener( "touchstart", (e) => {
                this.OnMouseDown();
        });
        window.addEventListener( "touchend", (e) => {           
                this.OnMouseUp();            
        });
        /* Comment these out when testing on mobile
        
        window.addEventListener("mousedown", (e) => {
            if (e.button == 0)
            {
                this.OnMouseDown();
            }
        });
        window.addEventListener("mouseup",  (e) => {
            if (e.button == 0)
            {
                this.OnMouseUp();
            }          
        });*/
    }
    OnMouseDown()
    {
        if (this.currentState == PlayerState.dead)
        {
            globalThis.Game.Restart();
        }
        else if (this.currentState == PlayerState.idle)
        {
            this.ChangeState(PlayerState.charge);
        }
    }
    OnMouseUp()
    {
        if (this.currentState == PlayerState.charge)
        {
            this.ChangeState(PlayerState.jump);
        }
    }
    public Restart() : void
    {
        let targetPlatform = globalThis.Game.platforms.GetPlayerPlatform();
        this.x = targetPlatform.x;
        this.LandOnPlatform(targetPlatform);
        this.ChangeState(PlayerState.idle);
        this.PlayAnimation(PlayerAnimation.ready,1);   
    }
    public ChangeState(newState : PlayerState) : void
    {
        if (newState == PlayerState.idle)
        {
            this.vel_x = 0;
            this.vel_y = 0;
            this.PlayAnimation(PlayerAnimation.idle,.66);   
            this.HideJumpIndocators(true);         
        }
        else if (newState == PlayerState.charge)
        {
            this.vel_x = 0;
            this.vel_y = -this.jump_height;
            this.vel_dir = Math.random() > .5 ? -1 : 1;
            this.HideJumpIndocators(false);
        }
        else if (newState == PlayerState.jump)
        {
            this.PlayAnimation(PlayerAnimation.jump,.66);      
            this.HideJumpIndocators(true);      
        }
        else if (newState == PlayerState.dead)
        {
            this.PlayAnimation(PlayerAnimation.idle,0);            
        }
        globalThis.Game.interface.HideGameOverText(newState != PlayerState.dead)
        this.currentState = newState;
    }
    HandleState() : void
    {
        if (this.currentState == PlayerState.jump)
        {
            this.Move();
            if (this.vel_y > 0)
            {
                this.ChangeState(PlayerState.fall);
            }
        }
        else if (this.currentState == PlayerState.charge)
        {
            this.AimJump();
            this.UpdateJumpIndicator();
        }
        else if (this.currentState == PlayerState.fall)
        {
            this.Move();
            let targetPlatform = globalThis.Game.platforms.GetMovingPlatform();
            if (this.IsOnPlatform(targetPlatform))
            {
                this.LandOnPlatform(targetPlatform);
                this.ChangeState(PlayerState.idle);
                globalThis.Game.OnPlayerScore();
            }
            else if (this.real_y > 200)
            {
                this.ChangeState(PlayerState.dead);
            }
        }
    }
    AimJump()
    {
        if (this.vel_dir == 0)
        {
            this.vel_dir = 1;
        }
        else if (this.vel_x < 0 - this.jump_height * .75)
        {
            this.vel_dir = 1;
        }
        else if (this.vel_x > this.jump_height * .75)
        {
            this.vel_dir = -1;
        }
        this.vel_x += this.jump_height * .1 * this.vel_dir;
    }
    override Move() : void
    {
        let nX = this.x + this.vel_x * .03;
        let nY = this.y + this.vel_y * .03;

        if (this.vel_x > 0)
        {
            let borderX = globalThis.Game.width / 2 - this.width / 2
            if (nX > borderX)
            {
                let velDelta = nX - borderX
                nX = borderX - velDelta;

                this.vel_x*=-.5;
            }
        }
        else 
        {
            let borderX = - globalThis.Game.width  / 2 + this.width / 2
            if (nX < borderX)
            {
                let velDelta = nX - borderX
                nX = borderX - velDelta;

                this.vel_x*=-.5;
            }
        }

        this.SetPosition(nX,nY);
        this.vel_y += this.jump_height * .03;
    }
    LandOnPlatform(targetPlatform : entityPlatform) : void
    {
        this.SetPosition(this.x, targetPlatform.real_y );
    }
    IsOnPlatform(targetPlatform : entityPlatform) : boolean
    {
        if (this.y < targetPlatform.real_y)
        {
            if (this.x > targetPlatform.x - targetPlatform.width / 2 && this.x < targetPlatform.x + targetPlatform.width / 2)
            {
                if (this.y + this.vel_y * .03 > targetPlatform.real_y)
                {
                    return true;
                }
            }
        }
        return false;
    }
    public SetPosition(nX,nY):void
    {
        super.SetPosition(nX,nY);
        this.real_x = globalThis.Game.width  / 2 + this.x;
        this.real_y = this.y - this.height / 2 - 12;
    }

    public UpdateVisual() : void
    {
        if (this.mySprite){
            let worldBottom = globalThis.Game.height + globalThis.Game.worldHeight
            this.mySprite.position.set(
                this.real_x ,
                worldBottom + this.real_y - this.height
                );
        }
        if (this.CharacterModel)
        {
            let modelX = this.real_x
            let modelY =  globalThis.Game.height + this.real_y + globalThis.Game.worldHeight - this.height/3
            this.CharacterModel.position.set(modelX,-modelY - 15,-400);

            if (this.currentState == PlayerState.idle)
            {
                this.CharacterModel.rotation.set(0,0,0);
            }
            else {
                let charRotation = this.currentState == PlayerState.fall ? 0 : .75;
                let desiredRotation = this.vel_x < 0 ? -charRotation : charRotation;
                this.CharacterModel.rotation.set(0,lerp(this.CharacterModel.rotation.y,desiredRotation,PlayerState.idle ? .33 : .05),0);
            }
        }
        if (this.mixer)
        {
            this.mixer.update(.03)
        }
    }
    Load3DModel()
    {
        const modelDir = new URL('../../resource/3D/cibus_ninja_fixed.glb',import.meta.url)
        
        const assetLoader = new GLTFLoader();
        
        let myPlayer = this;
        assetLoader.load(modelDir.href,function ( gltf ) {
        
            myPlayer.CharacterModel = gltf.scene;
            globalThis.Game.canvas3D.add( myPlayer.CharacterModel );
        
            myPlayer.CharacterModel.scale.set(myPlayer.width / 2,myPlayer.width / 2,myPlayer.width / 2);
            
            myPlayer.mixer = new THREE.AnimationMixer(myPlayer.CharacterModel)
            myPlayer.animations = gltf.animations;
            
            myPlayer.PlayAnimation(PlayerAnimation.ready,1);
        
        }, undefined, function ( error ) {
        
            console.error( error );
        
        } );
    }
    PlayAnimation(a : PlayerAnimation, speed : number)
    {
        if (this.mixer)
        {
            this.mixer.timeScale = speed;
            this.mixer.stopAllAction ()
                .clipAction(this.animations[a as number])                    
                .play()
        }
    }
}