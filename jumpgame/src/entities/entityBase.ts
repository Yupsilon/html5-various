/*     -----  BASE ENTITY -----

Base class that contains all the common properties of entities.
Values:
 - x,y: the coordinates of this entity
 - real_x,real_y: the coordinates of this entity relative to world bounds
 - width,height: the size of the entity
 - scale: the percentage size value of the entity
Functions:
 - SetScale: Changes the width, height and scale of this entity
 - Move: Moves this entity, abstract function
 - SetPosition: Changes the position of this entity
 - Update: Called each frame
 - UpdateVisual: Called when entity is drawn
*/

export default abstract class entityBase
{
    public x : number = 0;
    public y : number = 0;
    
    public real_x : number = 0;
    public real_y : number = 0;
    
    abstract width : number;
    abstract height : number;
    public scale : number = 1;
    
    SetScale(scale:number):void
    {
        let delta = scale / this.scale;

        this.width *= delta;
        this.height *= delta;

        this.scale = scale;        
    }
    Move() : void
    { }
    public SetPosition(nX,nY):void
    {
        this.x = nX;
        this.y = nY;
    }
    public Update() : void
    { }
    public UpdateVisual() : void
    { }
}