/*     -----  UTILS -----

This class contains basic mathematic functions.
 - lerp (a,b,c): Linearly interpolates between two values
 - clamp (val, min, max): restricts a number between the min and max values
*/

export function lerp(a : number,b : number,c : number) : number
{
    c = clamp(c,-1,1);
    return a * (1 - c) + b * c;
}
export function clamp (val : number, min : number, max : number) : number
{
    return Math.min(max, Math.max(min, val));
}