/*     -----  GAME VIEW -----

Class responsible for keeping track of player score and updating interface
Values:
 - width: width of the game stage
 - height: height of the game stage
 - difficulty: numeric value that affects the speed of platforms
 - worldHeight: current height of the player/camera
 - platformHeight: the distance between each platform
 - cameramoving: boolean value to check if the camera is moving
 - gamecanvas2D: the canvas containing the game 2D elements
 - canvas3D: the canvas containing 3D elements
 - score: the active score controller
 - background: the active background manager
 - platforms: the current platform manager
 - player: the main player entity
 - interface: the current interface manager
Functions:
 - Initialize: Initializes the game components
 - Restart: Restarts all the game components to the beginning of the game
 - initCanvases: Initializes the PIXI and THREE canvases, cameras, etc
 - UpdateFrame: Calls OnFrameUpdate and tasks a new UpdateFrame
 - OnFrameUpdate: Updates all game elements
 - MoveCamera: After the score updates, gradually moves the camera upwards
 - OnPlayerScore: Increases the score and difficulty, when the player scores
*/

import * as PIXI from 'pixi.js';
import * as THREE from 'three';
import BackgroundManager from './BackgroundManager'
import PlatformManager from './PlatformManager'
import entityPlayer from './entities/entityPlayer'
import ScoreController from './ScoreController';
import InterfaceManager from './InterfaceManager';

class GameView
{
    width : number = 1;
    height : number = 1;
    difficulty : number = 1;

    worldHeight : number = 0;
    platformHeight : number = 0;
    cameramoving : boolean = false;

    gamecanvas2D : PIXI.Application;
    canvas3D : THREE.Scene;

    score : ScoreController;
    background : BackgroundManager;
    platforms : PlatformManager;
    player : entityPlayer;
    interface : InterfaceManager;

    constructor(w,h)
    {
        this.height = h;
        this.width = Math.min(w,h);
    }
    Initialize():void
    {
        this.initCanvases();
        this.interface = new InterfaceManager();
        this.background = new BackgroundManager();
        this.platforms = new PlatformManager();
        this.player = new entityPlayer();
        this.score = new ScoreController();
        this.Restart();

        this.UpdateFrame();
    }
    
    Restart():void
    {
        this.cameramoving = false;
        this.difficulty = 1;
        this.worldHeight = 0;
        this.MoveCamera();
        this.platformHeight = this.height * .55;
        this.score.Reset();
        if (this.platforms)
        {
            this.platforms.ResetPlatforms();
        }
        if (this.player)
        {
            this.player.Restart();
        }
    }

    initCanvases():void
    {
        this.init_2D_canvas();
        this.init_3D_canvas();
    }

    init_2D_canvas():void
    {
        this.gamecanvas2D = new PIXI.Application({
            width : this.width,
            height : this.height,
            backgroundColor: 0x2980b9,
        });
        
        document.body.appendChild(this.gamecanvas2D.view);
        this.gamecanvas2D.view.style.position = "absolute";        
    }
    
    init_3D_canvas():void
    {
        const render = new THREE.WebGLRenderer({ alpha: true });
        render.setSize(this.width, this.height);
        
        document.body.appendChild(render.domElement);
        render.domElement.style.position = "absolute";
        render.domElement.style.zIndex = 9;
        
        this.canvas3D = new THREE.Scene();
        let size = 10;
        let camera = new THREE.OrthographicCamera(
            0,this.width,0,-this.height,
            -1,
            1000
        );
        this.canvas3D.add(camera);

        const axisHelper = new THREE.AxesHelper(10);
        this.canvas3D.add(axisHelper);
        
        var ambientLight = new THREE.AmbientLight( 0xFFFFFFFF);
        ambientLight.intensity = 2;
        this.canvas3D.add(ambientLight);

        const scene = this.canvas3D;
        function UpdateR(){
            render.render(scene,camera);
        }
        
        render.setAnimationLoop(UpdateR);
    }

    OnFrameUpdate():void
    {
        if (this.platforms)
        {
            this.platforms.UpdateMotion();
        }
        if (this.player)
        {
            this.player.HandleState();
        }
        this.MoveCamera();
    }
    UpdateFrame():void
    {
        const game = this;
        game.OnFrameUpdate();
        setTimeout(function(){
            game.UpdateFrame();
        },30);
    }
    MoveCamera():void
    {
        if (this.cameramoving == true)
        {
            let desiredWorldHeight = this.score.score * this.platformHeight;
            if (this.worldHeight < desiredWorldHeight)
            {
                this.worldHeight = Math.min(this.worldHeight + this.platformHeight * .05, desiredWorldHeight );
            }
            else 
            {
                this.cameramoving = false;
            }
        }
    }
    public OnPlayerScore() : void
    {
        this.score.IncreaseScore(1);
        this.difficulty += .02;
        this.platforms.Advance();
        this.cameramoving = true;
    }
}
globalThis.Game = new GameView(window.innerWidth, window.innerHeight);
globalThis.Game.Initialize();