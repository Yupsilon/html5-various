/*     -----  PLATFORM MANAGER -----

Class responsible for updating the height and positions of cloud paltforms
Values:
 - movingPlatforms: a pool of all the platforms in the game
Functions:
 - InitPlatforms: Initializes the platforms
 - ResetPlatforms: Resets the platforms to the initial positions when the game begins
 - UpdateMotion: Updates the motion on all platforms
 - Advance: Moves the platforms up, and changes the order of the platforms
 - GetMovingPlatform: Gets the platform that is currently moving
 - GetPlayerPlatform: Get the platform below the player
 - GetIdlePlatform: Get the unused, disabled platform
*/

import entityPlatform from "./entities/entityPlatform";

export default class PlatformManager {
    public movingPlatforms;
    public constructor()
    {
        this.InitPlatforms()
    }
    InitPlatforms() : void
    {
        this.movingPlatforms = {}
        this.movingPlatforms[0] = new entityPlatform();
        this.movingPlatforms[1] = new entityPlatform();
        this.movingPlatforms[2] = new entityPlatform();

    }
    public ResetPlatforms():void
    {
        this.movingPlatforms[0].SetPosition(0,1)
        this.movingPlatforms[0].moveDirection = 0;
        this.movingPlatforms[1].SetPosition(0,0)
        this.movingPlatforms[1].moveDirection = 0;
        this.movingPlatforms[2].SetPosition(0,-1)
        this.movingPlatforms[2].moveDirection = 1;
    }
    public UpdateMotion():void
    {
        for (var i = 0; i < 3; i++)
        {
            this.movingPlatforms[i].Update()
        }
    }
    public Advance() : void
    {
        let newIdlePlatform = this.GetPlayerPlatform();
        let newPlayerPlatform = this.GetMovingPlatform();
        let newMovingPlatform = this.GetIdlePlatform();

        newMovingPlatform.SetPosition(Math.random() * globalThis.Game.width / 2,newPlayerPlatform.y-1);        
        newMovingPlatform.moveDirection = (Math.random() > .5) ? 1 : -1;

        newPlayerPlatform.moveDirection = 0;
        newIdlePlatform.moveDirection = 0;
        
        this.movingPlatforms[0] = newIdlePlatform;
        this.movingPlatforms[1] = newPlayerPlatform;
        this.movingPlatforms[2] = newMovingPlatform;
    }
    public GetMovingPlatform():entityPlatform
    {
        return this.movingPlatforms[2];
    }
    public GetPlayerPlatform():entityPlatform
    {
        return this.movingPlatforms[1];
    }
    public GetIdlePlatform():entityPlatform
    {
        return this.movingPlatforms[0];
    }
}