//////console.log("Starting");

//Define Types
var Type_GameBorders = 0;	//Width, Height, Camera Start X,Y
var Type_GamePlatform = 1;	//X,Y,W,H,Type
var Type_GameEntity = 2;	//X,Y,Side,Type
var Type_Misc = 3;

//Fonts and UI elements
var normalFont = 'Arial';
var fancyFont = 'Comic Sans';

//Define Global Variables
var game = {
	FPS: 60,
	canvas: document.getElementById("canvas").getContext("2d"),
	width:document.getElementById("canvas").width,
	height:document.getElementById("canvas").height,
    toggleMusicBtn: document.getElementById("togglemusic"),
	tileSize:100,
	DrawRect:{xMin:0,yMin:0,xMax:document.getElementById("canvas").width,yMax:document.getElementById("canvas").height},
	runningGame:null,
}

var controls = {
	pause:32,
	selection:[49,50,51],	
}

var GetLevel = function()
{
	return Math.round(Math.random()*(StageData.length-1));
}

//Settings, hard coded
var AnimationNames = ['jump','fall','stand','walk','hurt','fly','spawn'];
var PlayerEntities = [0,1,2];
var respawnTime = 5;
var AiUpdateTime = 15;
var UltimateChargeRate = 1/4;
var Names = ['A','D','K']

var StageData = [
	[	//Stage 1
		[0,1150,450,0,0],
		
		[1,280,220,100,100,false,"wood"],
		[1,550,250,100,50,false,"wood"],		
		[1,820,220,100,100,false,"wood"],
		
		[1,650,280,400,200,true,"dirt"],
		[1,150,280,400,200,true,"dirt"],	
		[1,0,230,150,300,true,"dirt"],	
		[1,1000,230,150,300,true,"dirt"],
				
		[2,30,200,0],
		[2,70,200,0],
		[2,110,200,0],
		[2,1120,0,1],
		[2,1080,0,1],
		[2,1040,0,1],
	],
	[	//Stage 2
		[0,1000,800,0,500],
		
		[1,300,450,400,50,false,"wood"],
		[1,200,500,600,100,false,"wood"],
		[1,100,550,350,50,false,"wood"],		
		[1,600,550,350,50,false,"wood"],
		
		[1,0,600,1000,200,true,"dirt"],
				
		[2,250,520,0],
		[2,150,520,0],
		[2,200,520,0],
		[2,750,520,1],
		[2,850,520,1],
		[2,800,520,1],
	],
	[	//Stage 3
		[0,800,600,0,600],
				
		[1,300,325,200,75,false,"wood"],		
		[1,150,400,500,25,true,"dirt"],
		[1,30,455,100,15,false,"wood"],
		[1,670,455,100,15,false,"wood"],
						
		[2,160,350,0],
		[2,190,350,0],
		[2,220,350,0],
		[2,640,350,1],
		[2,610,350,1],
		[2,580,350,1],
	],
];

var entityData = [
	// 0 - Archer
	{
		width:25,
		height:36,
		Armor:5,
		Weight:2,
		StaminaRegen:0.25,
		Mesh:'char_Archer',
		Description:'...',
		FallSpeed:8,
		MoveSpeed:5,
		JumpRange:250,
		hitboxWidth:10,
		animationData:[
			[1],//Jump
			[2],//Fall
			[0],//Stand
			[3,0,4,0],//Walk
			[10],//Hurt
			[2],//Unused
			[5],//Spawn
		],
		Spells:[
		{// Power 0 - Bow Attack
			animData:[6,6],
			animSpeed:3,
			cost:0,
			range:250,
			angle:1,
			target:false,
			name:"Arrow",
			spellData:[
				{
					frame:1,
					spell:function(spellData)
					{
						var point = {x:spellData.x,y:spellData.y};
						if (spellData.target!=undefined)
						{
							point = {x:spellData.target.GetAttributes().x,y:spellData.target.GetAttributes().y};
						}
												
						game.runningGame.launchProjectileFromUnitToTarget(spellData.caster,
						'Arrow',
						0,//offset x
						4,//offset y
						point,//target point
						10,//speed
						15,//radius
						0.5,//weight
						15,//damage
						1/2,//lifetime
						true,//collides
						0,//explosion
						0,0,false);//stun,KB,pierce		
						PlaySound('bow.wav');
					}
				}
			]
		},
		{// Power 1 - Sword Attack
			animData:[8,9],
			animSpeed:1,
			cost:0,
			range:0,
			angle:0,
			target:true,
			name:"Dagger",
			spellData:[
				{
					frame:1,
					spell:function(spellData)
					{
						var damage = 20;
						if (Math.random()*2>1){
							damage=40;
						}
						spellData.caster.DealDamage(spellData.target,damage);
						PlaySound('sword.wav');
					}
				}
			]
		},
		{// Power 2 - Snipe
			animData:[6,6,6],
			animSpeed:5,
			cost:33,
			range:200,
			angle:1,
			target:false,
			name:"Magic Arrow",
			spellData:[
				{
					frame:2,
					spell:function(spellData)
					{						
						var point = {x:spellData.x,y:spellData.y};
						if (spellData.target!=undefined)
						{
							point = {x:spellData.target.GetAttributes().x,y:spellData.target.GetAttributes().y};
						}						
						
						game.runningGame.launchProjectileFromUnitToTarget(spellData.caster,
						'MagicArrow',
						0,//offset x
						4,//offset y
						point,//target point
						10,//speed
						30,//radius
						0.5,//weight
						15,//damage
						1/2,//lifetime
						false,//collides
						0,//explosion
						0.5,5,true);//stun,KB,pierce		
						PlaySound('magic.mp3');					
					}
				}
			]
		},
		{// Power 3 - Volley
			animData:[7,7,7],
			animSpeed:2,
			cost:100,
			range:200,
			angle:1,
			target:false,
			name:"Volley of Arrows",
			spellData:[
				{
					frame:1.8,
					spell:function(spellData)
					{
						var lifespan = 4/5;
						var damage = 20;	
						PlaySound('bow.wav');	
						
						game.runningGame.launchProjectileFromUnit(spellData.caster,
						'Arrow',//type
						0,//offsetx
						-15,//offsety
						4+Math.random()*4,//vx
						-20,//vy
						10,//rad
						1.4,//weight
						damage,//damage
						lifespan,//lifespan
						false,//collides
						0,//explosion
						0,0,false);//stun,KB,pierce
						
						game.runningGame.launchProjectileFromUnit(spellData.caster,
						'Arrow',//type
						0,//offsetx
						-15,//offsety
						4+Math.random()*4,//vx
						-20,//vy
						10,//rad
						1.4,//weight
						damage,//damage
						lifespan,//lifespan
						false,//collides
						0,//explosion
						0,0,false);//stun,KB,pierce	
							
					}
				},{
					frame:2,
					spell:function(spellData)
					{
						var lifespan = 4/5;
						var damage = 7;	
						PlaySound('bow.wav');	
																	
						game.runningGame.launchProjectileFromUnit(spellData.caster,
						'Arrow',//type
						0,//offsetx
						-15,//offsety
						4+Math.random()*4,//vx
						-20,//vy
						10,//rad
						1.4,//weight
						damage,//damage
						lifespan,//lifespan
						false,//collides
						0,//explosion
						0,0,true);//stun,KB,pierce
																	
						game.runningGame.launchProjectileFromUnit(spellData.caster,
						'Arrow',//type
						0,//offsetx
						-15,//offsety
						4+Math.random()*4,//vx
						-20,//vy
						10,//rad
						1.4,//weight
						damage,//damage
						lifespan,//lifespan
						false,//collides
						0,//explosion
						0,0,true);//stun,KB,pierce	
					}
				},{
					frame:2.2,
					spell:function(spellData)
					{
						var lifespan = 4/5;
						var damage = 7;	
						PlaySound('bow.wav');							
						
						game.runningGame.launchProjectileFromUnit(spellData.caster,
						'Arrow',//type
						0,//offsetx
						-15,//offsety
						4+Math.random()*4,//vx
						-20,//vy
						10,//rad
						1.4,//weight
						damage,//damage
						lifespan,//lifespan
						false,//collides
						0,//explosion
						0,0,true);//stun,KB,pierce
							
					}
				}
			]
		},
		{// Ultimate
			animData:[5,5,5],
			animSpeed:1,
			cost:0,
			range:0,
			ultimate:true,
			target:false,
			name:"Ulti",
			spellData:[
				{
					frame:0,
					noTarget:true,
					spell:function(spellData)
					{
						spellData.caster.AddModifier('overpower',5)
					}
				}
			]
		},
		],
	},
	// 1 - Dwarf
	{
		width:25,
		height:36,
		Armor:2.5,
		Weight:1.5,
		StaminaRegen:0.35,
		Mesh:'char_Dwarf',
		Description:'...',
		FallSpeed:10,
		MoveSpeed:5,
		JumpRange:200,
		hitboxWidth:10,
		animationData:[
			[1],//Jump
			[2],//Fall
			[0],//Stand
			[3,0,4,0],//Walk
			[7],//Hurt
			[2],//Unused
			[5],//Spawn
		],
		Spells:[
		{// Power 0 - Firebrew
			animData:[6,6,6],
			animSpeed:5,
			cost:0,
			range:200,
			angle:1,
			target:false,
			name:"Firebrew",
			spellData:[
				{
					frame:2,
					spell:function(spellData)
					{						
						var point = {x:spellData.x,y:spellData.y};
						if (spellData.target!=undefined)
						{
							point = {x:spellData.target.GetAttributes().x,y:spellData.target.GetAttributes().y};
						}						
						
						game.runningGame.launchProjectileFromUnitToTarget(spellData.caster,
						'Bomb',
						-10,//offset x
						10,//offset y
						point,//target point
						4,//speed
						15,//radius
						25,//weight
						15,//damage
						1,//lifetime
						true,//collides
						15,//explosion
						0,0,false);//stun,KB,pierce	
						PlaySound('throw.wav');						
					}
				}
			]
		},
		{// Power 1 - Flashbang
			animData:[6,6,6],
			animSpeed:5,
			cost:50,
			range:300,
			angle:1,
			target:false,
			name:"Firecracker",
			spellData:[
				{
					frame:2,
					spell:function(spellData)
					{						
						var point = {x:spellData.x,y:spellData.y};
						if (spellData.target!=undefined)
						{
							point = {x:spellData.target.GetAttributes().x,y:spellData.target.GetAttributes().y};
						}						
						
						game.runningGame.launchProjectileFromUnitToTarget(spellData.caster,
						'Flashbang',
						-10,//offset x
						10,//offset y
						point,//target point
						4,//speed
						10,//radius
						25,//weight
						20,//damage
						5,//lifetime
						true,//collides
						80,//explosion
						2,0,false);//stun,KB,pierce		
						PlaySound('throw.wav');										
					}
				}
			]
		},
		{// Power 2 - Big Boomer
			animData:[6,6,6],
			animSpeed:5,
			cost:100,
			range:200,
			angle:1,
			target:false,
			name:"Megabomb",
			spellData:[
				{
					frame:2,
					spell:function(spellData)
					{						
						var point = {x:spellData.x,y:spellData.y};
						if (spellData.target!=undefined)
						{
							point = {x:spellData.target.GetAttributes().x,y:spellData.target.GetAttributes().y};
						}						
						
						game.runningGame.launchProjectileFromUnitToTarget(spellData.caster,
						'Bomb',
						0,//offset x
						10,//offset y
						point,//target point
						4,//speed
						25,//radius
						12,//weight
						25,//damage
						1,//lifetime
						true,//collides
						80,//explosion
						0.5,10,false);//stun,KB,pierce	
						PlaySound('throw.wav');											
					}
				}
			]
		},
		{// Ultimate
			animData:[5,5,5],
			animSpeed:1,
			cost:0,
			range:0,
			ultimate:true,
			target:false,
			name:"Ulti",
			spellData:[
				{
					frame:0,
					noTarget:true,
					spell:function(spellData)
					{
						spellData.caster.AddModifier('overpower',5)
					}
				}
			]
		},
		],
	},
	// 3 - Knight
	{
		width:33,
		height:36,
		Armor:10,
		Weight:2,
		StaminaRegen:0.3,
		Mesh:'char_Knight',
		Description:'...',
		FallSpeed:8,
		MoveSpeed:5,
		JumpRange:180,
		hitboxWidth:10,
		animationData:[
			[1],//Jump
			[2],//Fall
			[0],//Stand
			[3,0,4,0],//Walk
			[6],//Hurt
			[2],//Unused
			[5],//Spawn
		],
		Spells:[
		{// Power 0 - Default Basic Attack
			animData:[7,8],
			animSpeed:0.8,
			cost:0,
			range:0,
			angle:0,
			target:true,
			name:"Sword Slash",
			spellData:[
				{
					frame:1,
					spell:function(spellData)
					{
						////console.log("Basic Attack");
						spellData.caster.DealDamage(spellData.target,15);
						if (Math.random()*3>2)
						{
							spellData.caster.KnockBack(spellData.target,5,5);
						}
						PlaySound('sword.wav')
					}
				}
			]
		},
		{// Power 1 - Regenerate
			animData:[9,9,9],
			animSpeed:0.6,
			cost:50,
			range:10000,
			angle:1,
			target:false,
			targetless:true,
			name:"Regenerate",
			spellData:[
				{
					frame:0.5,
					spell:function(spellData)
					{
						////console.log("Cast Spell");
						spellData.caster.DealHealing(spellData.caster,20);
						PlaySound('eat.wav');					
					}
				},
				{
					frame:1.5,
					spell:function(spellData)
					{
						////console.log("Cast Spell");
						spellData.caster.DealHealing(spellData.caster,20);
					}
				}
			]
		},
		{// Power 2 - Shockwave
			animData:[7,8],
			animSpeed:0.5,
			cost:100,
			range:0,
			angle:1,
			target:false,
			name:"Shockwave",
			spellData:[
				{
					frame:1,
					spell:function(spellData)
					{
						game.runningGame.launchProjectileFromUnit(spellData.caster,
						'Shockwave',//type
						0,//offsetx
						6,//offsety
						7,//vx
						0,//vy
						40,//rad
						0,//weight
						30,//damage
						0.13,//lifespan
						false,//collides
						0,//explosion
						1,10,true);//stun,KB,pierce	
						PlaySound('sword.wav')
					}
				}
			]
		},
		{// Ultimate
			animData:[5,5,5],
			animSpeed:1,
			cost:0,
			range:0,
			ultimate:true,
			target:false,
			name:"Ulti",
			spellData:[
				{
					frame:0,
					noTarget:true,
					spell:function(spellData)
					{
						spellData.caster.AddModifier('overpower',5)
					}
				}
			]
		},
		],
	},
];

var RectContainsRect = function(rA,rB)
{	
	return !(rB.xMin > rA.xMax || 
           rB.xMax < rA.xMin || 
           rB.yMin > rA.yMax ||
           rB.yMax < rA.yMin);
}

var RectContainsPoint = function(rect,point)
{	
	return (point.x<rect.xMax && point.x>rect.xMin && point.y<rect.yMax && point.y>rect.yMin);
}
		
var entityPlayer = function()
{	
	var ID=0;
	var playerunits = [];
	var SelectedEntities = [];
	var isAI = false;
	var UltimatePercentage = 0;
	var spawners = [];
	var CameraPosition = [0,0];
	var Score=0;
	
	var getUltimate = function()
	{
		return UltimatePercentage;
	}
	var setUltimate = function(value,total)
	{
		UltimatePercentage=UltimatePercentage+value;
		if (total)
		{
			UltimatePercentage=value;
		}
	}
	
	var solveAiPlayer = function()
	{
		////console.log("Handling Ai Player")
		for (var eEnt = 0; eEnt<playerunits.length; eEnt++)	
		{
			var eUnit = playerunits[eEnt];
			
			if (eUnit.CanAct())
			{				
				////console.log("Handling Unit "+eEnt)
				var eUnitData = eUnit.GetAttributes();
				
				//Try Harass Enemy Units
				var myRange = 200;
				var pPower = 0;
				
				for (var iAbility=0; iAbility<eUnitData.Spells.length; iAbility++)
				{
					if (eUnitData.Spells[iAbility].cost<=eUnitData.Stamina && eUnitData.Spells[iAbility].cost>eUnitData.Spells[pPower].cost)
					{
						myRange=Math.max(eUnitData.hitboxWidth*2,eUnitData.Spells[iAbility].range);
						pPower=iAbility;
					}
				}
				
				var myTargets=game.runningGame.PickUnitsInRange(eUnitData.x,eUnitData.y,myRange,1-ID);				
				
				if (myTargets.length>0)
				{
					////console.log("Found target! Harassing!")
					if (getUltimate()>=100)
					{
						eUnit.CastUltimate();
						setUltimate(0,true);
					}
					else
					{
						var targetTarget = myTargets[Math.round((myTargets.length-1)*Math.random())];
					
						//Am I in range?
						var Dist = Math.sqrt((targetTarget.GetAbsolutePosition().x-eUnit.GetAbsolutePosition().x)*(targetTarget.GetAbsolutePosition().x-eUnit.GetAbsolutePosition().x)+(targetTarget.GetAbsolutePosition().y-eUnit.GetAbsolutePosition().y)*(targetTarget.GetAbsolutePosition().y-eUnit.GetAbsolutePosition().y));
						
						if (Dist>Math.max(eUnitData.hitboxWidth,eUnitData.Spells[pPower].range) && Dist<=eUnitData.JumpRange && Dist>eUnitData.JumpRange/2)
						{
							eUnit.AddOrder({id:'jump',x:targetTarget.GetAbsolutePosition().x,y:targetTarget.GetAbsolutePosition().y},true);
						}
						else
						{
							//console.log("Casting "+eUnitData.Spells[pPower].name)
							eUnit.AddOrder({id:'power',data:pPower,target:targetTarget,repeat:eUnitData.Spells[pPower].cost==0},true);
						}						
					}
				}				
				else //Otherwise move to the closest entity
				{
					//console.log("Try to move closer!");
					
					var allTargets=game.runningGame.PickUnitsInRange(eUnitData.x,eUnitData.y,-1,1-ID);
					var Dist = 0;
					var moveTarget = null;
					
					for (var iTarget = 0; iTarget<allTargets.length; iTarget++)
					{
						if (moveTarget==null || Math.sqrt((moveTarget.GetAbsolutePosition().x-eUnit.GetAbsolutePosition().x)*(moveTarget.GetAbsolutePosition().x-eUnit.GetAbsolutePosition().x)+(moveTarget.GetAbsolutePosition().y-eUnit.GetAbsolutePosition().y)*(moveTarget.GetAbsolutePosition().y-eUnit.GetAbsolutePosition().y))<Dist)
						{
							moveTarget=allTargets[iTarget];
							Dist=Math.sqrt((moveTarget.GetAbsolutePosition().x-eUnit.GetAbsolutePosition().x)*(moveTarget.GetAbsolutePosition().x-eUnit.GetAbsolutePosition().x)+(moveTarget.GetAbsolutePosition().y-eUnit.GetAbsolutePosition().y)*(moveTarget.GetAbsolutePosition().y-eUnit.GetAbsolutePosition().y));
						}
					}
					
					if (moveTarget==null)
					{
						////console.log("No target to chase");
						return;
					}
					
					if (moveTarget.GetAbsolutePosition().y!=eUnit.GetAbsolutePosition().y)
					{
						eUnit.AddOrder({id:'jump',x:eUnit.GetAbsolutePosition().x,y:eUnit.GetAbsolutePosition().y-eUnit.JumpRange},true);
					}
					
					var moveDirection = 0;					
					
					if (eUnitData.x>moveTarget.GetAttributes().x)			
					{
						moveDirection=1;
					}
					else
					{
						moveDirection=-1;
					}
					
					if (eUnitData.myPlatform!=null)
					{						
						//Walk to the edge of the platform
						var platform = eUnitData.myPlatform.GetPlatformData();
						if (moveDirection<0 && Math.abs(platform.x+platform.w-eUnitData.x)>=eUnitData.hitboxWidth*2) 
						{
							////console.log("Move to platform edge")
							eUnit.AddOrder({id:'move',x:platform.x+platform.w,y:platform.y},true);
						}
						else if (moveDirection>0 && Math.abs(platform.x-eUnitData.x)>=eUnitData.hitboxWidth*2)
						{
							////console.log("Move to platform edge")
							eUnit.AddOrder({id:'move',x:platform.x,y:platform.y},true);
						}
						else
						{
							//console.log("Get closest available platform")
							var myPlatform=null;
							
							for (var pPlatform = 0; pPlatform<game.runningGame.GetAllPlatforms().length; pPlatform++)
							{
								var tempPlatform = game.runningGame.GetAllPlatforms()[pPlatform].GetPlatformData();
								
								if (myPlatform==null && tempPlatform!=eUnitData.myPlatform)
								{
									myPlatform=tempPlatform;
								}
								else if (moveDirection<0)
								{
									if (tempPlatform.x+tempPlatform.w/2>eUnitData.x)
									{
										if (tempPlatform.x<myPlatform.x)
										{
											myPlatform=tempPlatform;
										}
									}
								}
								else if (moveDirection>0)
								{
									if (tempPlatform.x+tempPlatform.w/2<eUnitData.x)
									{
										if (tempPlatform.x>myPlatform.x)
										{
											myPlatform=tempPlatform;
										}
									}
								}
							}
							
							if (myPlatform!=null) {
								if (moveDirection>0)
									{
										eUnit.AddOrder({id:'jump',x:myPlatform.x,y:myPlatform.y},true);
									}
									else if (moveDirection<0)
									{
										eUnit.AddOrder({id:'jump',x:myPlatform.x+myPlatform.w,y:myPlatform.y},true);
									}
							}
						}
					}
				}
			}
		}
	}
	
	var setID = function(toWhat)
	{
		ID = toWhat;
	}
	
	return {
	setID:setID,
	getID:function(){return ID},
	Score:Score,
	spawners:spawners,
	playerunits:playerunits,			
	SelectedEntities:SelectedEntities,
	isAI:isAI,
	getUltimate:getUltimate,
	setUltimate:setUltimate,
	CameraPosition:CameraPosition,
	solveAiPlayer:solveAiPlayer,
	}
}
		
		
		
var entityGame = function() 
{
	var FrameDraw = 0;
	
	var gameobjects = [];
	var gametiles = [];
	var gameplayers = [];
	var myPlayer = 0;
	var gamepaused=false;
	var myWinner = -1;
	
	var gLoop;
	var GameTime=0;
	var GameTick = 0;
	var GameSpeed = 30/game.FPS;
	var LastAiTime = 0;
	
	var Level = 0;
	var gameMode="deathmatch";
	var ScoreToWin = 10;	
	var Replenish = true;
	
	var MouseData = {
		mouseCenterX:-1,
		mouseCenterY:-1,
		mouseDragBox:false,
		mousePowerUp:false,
		mouseBoxX:-1,
		mouseBoxY:-1,
		lastClickTime:-1,
	}
	var PowerOverlay = {
		enabled:false,
		drawn:false,
		x:0,
		y:0,
		width:0,
		height:0,
		caster:null,
		target:null,
		buttons:[],
	}
	
	var GameBorders = [600,400,0,0];
	
	var initialize = function(LevelData)
	{
		gamepaused=true;	
		FrameDraw = 0;
		gameobjects = [];
		gametiles = [];
		gameplayers = [];
		myPlayer = 0;
		GameTime=0;
		GameTick = 0;
		LastAiTime = 0;
	
		//HardCoded Data
	
		Level = 0;
		gameMode="deathmatch";
		
		myWinner=-1;
		GameTime = 0;
		GameSpeed = 1;
		Level=LevelData;
		DoThePlayers();
		DrawTheGameEntities();
		
        document.getElementById("canvas").onmousedown = onMouseDown;		
        document.getElementById("canvas").onmouseup = onMouseUp;
        document.getElementById("canvas").onmousemove = onMouseMove; 
		
		gamepaused = false;
		GameSpeed = 30/game.FPS;
		Update();
	
	}
		
	var MoveCamera= function(iX,iY)
	{
		
		iX-=game.width/2;
		iY-=game.height/2;
		
		gameplayers[myPlayer].CameraPosition=[
		Math.max(0,Math.min(iX,GameBorders[0]-game.width)),
		Math.max(0,Math.min(iY,GameBorders[1]-game.height))
		];
		
		
		game.DrawRect = 
		{
			xMin:gameplayers[myPlayer].CameraPosition[0],
			xMax:gameplayers[myPlayer].CameraPosition[0]+game.width,
			yMin:gameplayers[myPlayer].CameraPosition[1],
			yMax:gameplayers[myPlayer].CameraPosition[1]+game.height,
		}
		
	}
	
	var DoThePlayers = function()
	{
		gameplayers = [];

		var PlayerPlayer = new entityPlayer();		
		PlayerPlayer.isAI = false;
		PlayerPlayer.setUltimate(0,true);
		PlayerPlayer.setID(0);
		gameplayers.push(PlayerPlayer);
		
		var enemyPlayer = new entityPlayer();		
		enemyPlayer.isAI = true;
		enemyPlayer.setID(1);
		enemyPlayer.setUltimate(0,true);
		gameplayers.push(enemyPlayer);
		
		myPlayer=0;
	}
	
	var onKeyPress = function(event)
	{
		//////console.log(event.keyCode);
		if (event.keyCode==controls.pause)
		{
			gamepaused=!gamepaused;
		}
		else if (controls.selection.indexOf(event.keyCode)!=-1)
		{
			gameplayers[myPlayer].SelectedEntities = [gameplayers[myPlayer].playerunits[controls.selection.indexOf(event.keyCode)]];
		}
	}
	
	var onMouseDown = function(event) 
	{
		if (PowerOverlay.drawn==true && PowerOverlay.enabled == true)
		{
			if (event.offsetX >= PowerOverlay.rx && event.offsetX <= PowerOverlay.rx+PowerOverlay.rwidth &&
			event.offsetY >= PowerOverlay.ry && event.offsetY <= PowerOverlay.ry+PowerOverlay.rheight)
			{
				for (var iBtn = 0; iBtn<PowerOverlay.buttons.length; iBtn++)
				{
					var Btn = PowerOverlay.buttons[iBtn];
					
					if (event.offsetX >= Btn.x && event.offsetX <= Btn.x+Btn.width &&
						event.offsetY >= Btn.y && event.offsetY <= Btn.y+Btn.height)
					{
						//////console.log(iBtn);
						Btn.onClick(Btn.bData);
						PowerOverlay.drawn=false;
						PowerOverlay.enabled=false;
						break;
					}					
				}
			}
			else
			{
				PowerOverlay.drawn=false;
				PowerOverlay.enabled=false;
			}
		}
		else 
		{
			MouseData.mouseDragBox=true;
			MouseData.mousePowerUp=false;
			
			var overGUI=false;
			
			//Am I Using The Ultimate
			if (gameplayers[myPlayer].getUltimate()>=100 &&
			event.offsetX>game.width/2-30 &&
			event.offsetX<game.width/2+30 &&
			event.offsetY<60)
			{
				MouseData.mousePowerUp=true;
				MouseData.mouseDragBox=false;
			}
			else {
				for (var iE=0;iE<3; iE++)
				{
					if (event.offsetX>20+40*iE &&
			event.offsetX<20+40*(iE+1) &&
			event.offsetY>game.height-40)
			{
				gameplayers[myPlayer].SelectedEntities = [gameplayers[myPlayer].playerunits[iE]];	
				overGUI=true;
			}
				}
			}
			
			MouseData.lastClickTime=GameTick;
			
			if (!overGUI){
			MouseData.mouseCenterX=event.offsetX+game.DrawRect.xMin;
			MouseData.mouseCenterY=event.offsetY+game.DrawRect.yMin;
			}
		}
    };
	
	var onMouseUp = function(event) 
	{
		
		if (myWinner<0)
		{
		
		MouseData.lastClickTime=-1;
		if (MouseData.mouseDragBox==true)
		{
			//Do Selection
			DoSelection();
		}
		else if (MouseData.mousePowerUp==true)
		{
			var eEnt = GetMouseEntity(true);
			if (eEnt!=null && eEnt.AmIPlayerControlled()==true && !eEnt.AmIDead())
			{
				gameplayers[myPlayer].setUltimate(0,true);
				eEnt.CastUltimate();
			}
		}
		//Reset Variables
		MouseData.mouseDragBox=false;
		MouseData.mousePowerUp=false;
		MouseData.mouseCenterX=-1;
		MouseData.mouseCenterY=-1;
		MouseData.mouseBoxX=-1;
		MouseData.mouseBoxY=-1;
		}
		else
		{
			clearTimeout(gLoop);
			StartNewGame();
		}
    };
	
	var onMouseMove = function(event) 
	{
		MouseData.mouseBoxX=event.offsetX+game.DrawRect.xMin;
		MouseData.mouseBoxY=event.offsetY+game.DrawRect.yMin;
    };
	
	var GetMouseEntity = function(absolute)
	{
		for (var iEnt = 0; iEnt<gameobjects.length; iEnt++)
		{
			var eEntity = gameobjects[iEnt];
				if (eEntity.GetType() == Type_GameEntity)
				{
					if (RectContainsPoint(eEntity.GetHitBox(),{x:MouseData.mouseBoxX,y:MouseData.mouseBoxY}))
					{						
						if (!eEntity.AmISelected() || absolute)
						{
							return eEntity;
						}						
					}
				}
			}
			return null;
	}
	
	var DoSelection = function()
	{
		var tempSelection = [];
		var MouseEntity = GetMouseEntity(false);
		
		////////console.log(MouseData);
		if (Math.abs(MouseData.mouseCenterX-MouseData.mouseBoxX)<10 && Math.abs(MouseData.mouseCenterY-MouseData.mouseBoxY)<10)
		{
			if (MouseEntity!=null && MouseEntity.AmIPlayerControlled())
			{
				//////console.log('Select Single Entity');
				tempSelection.push (MouseEntity);
			}							
		}
		else 
		{
			//////console.log('Select Box');
			for (var iEnt = 0; iEnt<gameobjects.length; iEnt++)
			{
				var eEntity = gameobjects[iEnt];
				if (eEntity.GetType() == Type_GameEntity && eEntity.AmIPlayerControlled())
				{
					if (RectContainsRect(
					eEntity.GetHitBox(),
					{
						xMin:Math.min(MouseData.mouseCenterX,MouseData.mouseBoxX),
						yMin:Math.min(MouseData.mouseCenterY,MouseData.mouseBoxY),
						xMax:Math.max(MouseData.mouseCenterX,MouseData.mouseBoxX),
						yMax:Math.max(MouseData.mouseCenterY,MouseData.mouseBoxY)
					}))
					{			
						tempSelection.push(eEntity);
					}
				}
			}
		}
		
		if (tempSelection.length>0)
		{				
			//First clear selection
			for (var iEnt = 0; iEnt<gameplayers[myPlayer].SelectedEntities.length; iEnt++)
			{
				var Selection = gameplayers[myPlayer].SelectedEntities[iEnt];
			}
			gameplayers[myPlayer].SelectedEntities=[];
			
			for (var iEnt = 0; iEnt<tempSelection.length; iEnt++)
			{
				var eEntity = tempSelection[iEnt];
									
				gameplayers[myPlayer].SelectedEntities.push(eEntity);								
			}
		}
		else
		{
			if (Math.abs(MouseData.mouseCenterX-MouseData.mouseBoxX)<10 && Math.abs(MouseData.mouseCenterY-MouseData.mouseBoxY)<10)
			{
				if (gameplayers[myPlayer].SelectedEntities.length>0)
				{
					if (MouseEntity==null)
					{
						//////console.log('Order Move');
						for (var iEnt = 0; iEnt < gameplayers[myPlayer].SelectedEntities.length; iEnt++)
						{
							var eEnt = gameplayers[myPlayer].SelectedEntities[iEnt];
							eEnt.AddOrder({id:'move',x:MouseData.mouseCenterX,y:MouseData.mouseCenterY},true);
						}
					}
					else if (!MouseEntity.AmIPlayerControlled())
					{						
						//////console.log('Order Attack');
						for (var iEnt = 0; iEnt < gameplayers[myPlayer].SelectedEntities.length; iEnt++)
						{
							var eEnt = gameplayers[myPlayer].SelectedEntities[iEnt];
							eEnt.AddOrder({id:'power',data:0,target:MouseEntity,repeat:true},true);
						}
					}
				}
			}
		}
	}
	var gameplatforms = [];
	
	var DrawTheGameEntities = function()
	{			
		for (var iEnt = 0; iEnt<StageData[Level].length; iEnt++)
		{
			var EntData = StageData[Level][iEnt];
			
			if (EntData[0]==Type_GameBorders)//Establish the Borders
			{
				GameBorders=[EntData[1],EntData[2],EntData[3],EntData[4]];				
				MoveCamera(GameBorders[2],GameBorders[3]);
			}
			else if (EntData[0]==Type_GamePlatform)//Draw the platforms
			{
				var pPlatform = new entityPlatform (iEnt,[EntData[1],EntData[2],EntData[3],EntData[4],EntData[5],EntData[6]]);
				gameobjects.push(pPlatform);
				gameplatforms.push(pPlatform);
			}
			else if (EntData[0]==Type_GameEntity)//Assign Spawners
			{				
					if (EntData[3]==gameplayers[myPlayer].getID())
					{
						gameplayers[myPlayer].spawners.push({x:EntData[1],y:EntData[2],lastUseTime:0});
					}
					else
					{					
						gameplayers[1-myPlayer].spawners.push({x:EntData[1],y:EntData[2],lastUseTime:0});
					}
			}
		}
		
		for (var iPlayer = 0; iPlayer<gameplayers.length; iPlayer++)
		{
			for (var iEnt = 0; iEnt<PlayerEntities.length; iEnt++)
			{
				var eData = new entityUnit (iEnt*(iPlayer+1),[-1,-1],iPlayer,PlayerEntities[iEnt],false);
				eData.setInstance(eData);
					gameobjects.push(eData);
					
					if (eData.AmIPlayerControlled())
					{
						gameplayers[myPlayer].playerunits.push(eData);
					}
					else
					{					
						gameplayers[1-myPlayer].playerunits.push(eData);
					}
			}
		}
		
		gametiles = [];
		for (var iY= 0; iY<Math.ceil(GameBorders[1]/game.tileSize); iY++)
		{
			gametiles.push([]);
			for (var iX= 0; iX<Math.ceil(GameBorders[0]/game.tileSize); iX++)
			{
				var tempTile = new entityTile();
				
				for (var iEnt = 0; iEnt<gameobjects.length; iEnt++)
				{
					var eEntity = gameobjects[iEnt];
					
					if (eEntity.GetType() == Type_GamePlatform && eEntity.AmIinTile(iX,iY))
					{
						tempTile.tileEntities.push(eEntity);
					}					
				}
				
				gametiles[iY].push(tempTile);				
			}
		}
	}
		
	var GetAllPlatforms= function()
	{
		return gameplatforms;
	}
	
	var PickUnitsInRange= function (rX,rY,rR,rS)
	{
		var complete = [];
		
		for (var iU =0; iU<gameobjects.length; iU++)
		{
			var eUnit = gameobjects[iU];
				
			if (eUnit.AmIValidUnit && !eUnit.AmIDead())
			{
				var eData = eUnit.GetAttributes();
				
				if (eData.Side==rS && (rR==-1 || Math.sqrt((eUnit.GetAbsolutePosition().x-rX)*(eUnit.GetAbsolutePosition().x-rX)+(eUnit.GetAbsolutePosition().y-rY)*(eUnit.GetAbsolutePosition().y-rY))<rR+eUnit.GetAbsolutePosition().r))
				{
					complete.push(eUnit);
				}
			}			
		}	
		return complete;		
	}
	
	var drawUserInterface = function()
	{	
		
		if (myWinner<0){
		//Score Indicators	
		var tSize = 15;
			game.canvas.fillStyle = 'rgba(0, 0, 0, 1)';				
				game.canvas.textAlign = "left";	
				game.canvas.font=tSize+"px "+normalFont;	
				
		if (gameMode=="deathmatch"){
		
				game.canvas.fillText("Score:",10,tSize);	
		
		for (var p = 0; p<gameplayers.length; p++)
		{		
	var ePlayer = gameplayers[p];
				game.canvas.fillText("Player "+(ePlayer.getID()+1)+": "+ePlayer.Score+"\n",10, (2+p)*tSize);	
		}
				
				
		}
	
		//Player Indicators
		for (var i = 0; i<gameplayers[myPlayer].playerunits.length; i++)
		{
			var eUnit = gameplayers[myPlayer].playerunits[i];
			var eData = eUnit.GetAttributes();
			
			//Stamina Circle
			game.canvas.fillStyle = 'rgba(100,100,255, 1)';				
			game.canvas.beginPath();			
			if (eData.Stamina==100)
			{
			game.canvas.arc(40*(1+i), game.height-30, 19, 0, Math.PI * 2, true);
			} else
			{
			game.canvas.arc(40*(1+i), game.height-30, 19, eData.Stamina/100*Math.PI*2, Math.PI * 2, true);
			}
			game.canvas.closePath();
			game.canvas.fill();						
			game.canvas.strokeStyle = 'rgba(0,0,0, 0)';
						
			//Background
			game.canvas.fillStyle = 'rgba(0,0,0, 1)';				
			game.canvas.beginPath();			
			game.canvas.arc(40*(1+i), game.height-30, 15, 0, Math.PI * 2, true);
			game.canvas.closePath();
			game.canvas.fill();	
			
			//Life Circle
			if (eData.Life>0)
			{
			game.canvas.fillStyle = 'rgba(50,200,50, 1)';				
			game.canvas.beginPath();			
			game.canvas.arc(40*(1+i), game.height-30, 15*eData.Life/100, 0, Math.PI * 2, true);
			game.canvas.closePath();
			game.canvas.fill();						
			game.canvas.lineWidth = 1;
			game.canvas.strokeStyle = 'rgba(0,0,0, 1)';
			game.canvas.stroke();
			
				game.canvas.fillStyle = 'rgba(255, 255, 255, 1)';				
				game.canvas.textAlign = "center";	
				game.canvas.font="20px "+normalFont;			
				game.canvas.fillText(Names[i],40*(1+i), game.height-22.5);	
			}
			else
			{
				game.canvas.fillStyle = 'rgba(255, 255, 255, 1)';				
				game.canvas.textAlign = "center";	
				game.canvas.font="20px "+normalFont;			
				game.canvas.fillText(eUnit.GetRespawnTime(),40*(1+i), game.height-22.5);					
			}
		}
		
		//Ultimate		
		var x = game.width/2;
		var r = 40;
		var y = r/2;
		
		if (MouseData.mousePowerUp==true)
		{
			x = MouseData.mouseBoxX-game.DrawRect.xMin;
			y = MouseData.mouseBoxY-game.DrawRect.yMin;				
		}
		
		game.canvas.drawImage(document.getElementById('UltimateCharge'), 
				0, 0, //x,y
				r, r, //w,h
				x-r/2*gameplayers[myPlayer].getUltimate()/100, y-r/2*gameplayers[myPlayer].getUltimate()/100,
				r*gameplayers[myPlayer].getUltimate()/100, r*gameplayers[myPlayer].getUltimate()/100);
		
		
		if (MouseData.mousePowerUp!=true)
		{
			var Size = r/3*(gameplayers[myPlayer].getUltimate()/100)
				game.canvas.fillStyle = 'rgba(255, 255, 255, 1)';				
				game.canvas.textAlign = "center";	
				game.canvas.font=Size+"px "+fancyFont;			
				game.canvas.fillText(Math.min(100,Math.floor(gameplayers[myPlayer].getUltimate()))+"%",x, y);	
		}
		
		if (gamepaused)
		{
				game.canvas.fillStyle = 'rgba(0, 0, 0, 1)';				
				game.canvas.textAlign = "center";	
				game.canvas.font="20px "+fancyFont;			
				game.canvas.fillText("Game Paused",game.width/2, game.height*4/5);					
		}
		
		}
		else
		{
			game.canvas.fillStyle = 'rgba(0, 0, 0, 1)';				
				game.canvas.textAlign = "center";	
				game.canvas.font="20px "+fancyFont;			
				game.canvas.fillText("Player "+(myWinner+1)+" Wins!",game.width/2, game.height/2);	
		}
	}
	
	var drawBackground = function()
	{
		game.canvas.fillStyle = '#EEEEEE';
		/*game.canvas.beginPath();
		game.canvas.rect(0, 0, game.width, game.height);
		game.canvas.closePath();
		game.canvas.fill();*/
		
		game.canvas.drawImage(document.getElementById("background"),0,0, game.width,game.height);
							
		game.canvas.lineWidth = 1;
		game.canvas.strokeStyle = 'rgba(0,0,0, 0)';
		game.canvas.stroke();
		
		var Grid = false;	
		
		if (Grid)
		{
			for (var tX =0; tX<GameBorders[0]/game.tileSize; tX++)
			{
				for (var tY =0; tY<GameBorders[1]/game.tileSize; tY++)
				{
					
					game.canvas.beginPath();
					game.canvas.rect(
						tX*game.tileSize-gameplayers[myPlayer].CameraPosition[0],
						tY*game.tileSize-gameplayers[myPlayer].CameraPosition[1],
						game.tileSize,
						game.tileSize
					);
					game.canvas.closePath();
							
					game.canvas.lineWidth = 1;
					game.canvas.strokeStyle = 'rgba(0,0,0, 0.2)';
					game.canvas.stroke();
				}
			}
		}
	}
			
	var drawSpellOverlay = function()
	{
		if (PowerOverlay.caster!=undefined)
		{			
			if (PowerOverlay.drawn!=true)
			{
				PowerOverlay.drawn=true;
				
				var Borders = 10;
				var casterData = PowerOverlay.caster.GetAttributes();
				
				PowerOverlay.buttons = [];
				var tempSpells = [];
				for (var iBtn = 0; iBtn<casterData.Spells.length; iBtn++)
				{
					if (casterData.Spells[iBtn].ultimate!=true &&
					(casterData.Spells[iBtn].target==false || (PowerOverlay.target!=undefined && PowerOverlay.target!=null)))
					{
						tempSpells.push(iBtn);
					}
				}
				
				var Max = tempSpells.length+1;
				
				var BoxSize =
				[
					120,(Max)*(25+Borders)+Borders
				];
							
				PowerOverlay.rwidth=BoxSize[0];
				PowerOverlay.rheight=BoxSize[1];
				PowerOverlay.rx=Math.max(0,Math.min(game.width-BoxSize[0],PowerOverlay.x-game.DrawRect.xMin));
				PowerOverlay.ry=Math.max(0,Math.min(game.height-BoxSize[1],PowerOverlay.y-game.DrawRect.yMin));
				
				for (var iBtn = 0; iBtn<Max; iBtn++)
				{					
					var fClick = null;
					var tData = "";
					var iI = -1;
					
					if (iBtn<tempSpells.length)
					{											
						iI = tempSpells[iBtn];
						fClick= function(ID) {PowerOverlay.caster.AddOrder({id:'power',data:ID,x:PowerOverlay.x,y:PowerOverlay.y,target:PowerOverlay.target},true);}
						tData = PowerOverlay.caster.GetAttributes().Spells[iI].name+" ("+ PowerOverlay.caster.GetAttributes().Spells[iI].cost +")";
					}
					else
					{
						fClick= function(ID) {PowerOverlay.caster.AddOrder({id:'jump',x:PowerOverlay.x,y:PowerOverlay.y},true);}
						tData = "Jump";
					}
			
					var tempButton =
					{
						x:PowerOverlay.rx+Borders,
						width:PowerOverlay.rwidth-Borders*2,
						y:PowerOverlay.ry+Borders+(25+Borders)*iBtn,
						height:25,
						bData:iI,
						name:tData,
						onClick:fClick,
					}
					PowerOverlay.buttons.push(tempButton);
				}
			}
			
			game.canvas.fillStyle = '#EEEEEE';
			game.canvas.beginPath();
			game.canvas.rect(PowerOverlay.rx, PowerOverlay.ry, PowerOverlay.rwidth, PowerOverlay.rheight);
			game.canvas.closePath();
			game.canvas.fill();
								
			game.canvas.lineWidth = 1;
			game.canvas.strokeStyle = 'rgba(255,0,0, 1)';
			game.canvas.stroke();
			
			for (var iBtn = 0; iBtn<PowerOverlay.buttons.length; iBtn++)
			{
				var Btn = PowerOverlay.buttons[iBtn];
				
				game.canvas.fillStyle = '#FFFFFF';
				game.canvas.beginPath();
				game.canvas.rect(Btn.x, Btn.y, Btn.width, Btn.height);
				game.canvas.closePath();
				game.canvas.fill();
				
				game.canvas.fillStyle = 'rgba(0, 0, 0, 1)';				
				game.canvas.textAlign = "center";	
				game.canvas.font="10px "+normalFont;			
				game.canvas.fillText(Btn.name,Btn.x+Btn.width/2, Btn.y+Btn.height/2);		

			}
		}
		else if (PowerOverlay.target!=undefined && PowerOverlay.target!=null)
		{
			var myCaster = PowerOverlay.target;
		}
	}
	
	var drawScreen = function ()
	{		
		/*var drawObjects = [];		//Old draw by tile
		var drawObjectsID = [];
		
		var xMin=Math.floor(CameraPosition[0]/game.tileSize);
		var xMax = Math.ceil((CameraPosition[0]+game.width)/game.tileSize);
		var yMin = Math.floor(CameraPosition[1]/game.tileSize);
		var yMax = Math.ceil((CameraPosition[1]+game.height)/game.tileSize);
				
		for (var iX= xMin; iX<xMax; iX++)
		{				
			for (var iY= yMin; iY<yMax; iY++)
			{
				var eTile = gametiles[iY][iX];								
					////////console.log(iX+"_"+iY + " "+eTile.tileEntities.length);
				for (var iEnt = 0; iEnt<eTile.tileEntities.length; iEnt++)
				{								
					var eEntity = eTile.tileEntities[iEnt];
					
					if (drawObjectsID.indexOf(eEntity.GetID())==-1)
					{
						drawObjects.push(eEntity);					
						drawObjectsID.push(eEntity.GetID());					
					}
				}
			}
		}*/
		
		for (var iObj =0; iObj<gameobjects.length; iObj++)			
		{
			var oObj = gameobjects[iObj];
			if (oObj.Draw!=undefined)
			{oObj.Draw()}			
		}
	}
	
	var Update = function()
	{			
		//Draw 		
		if (FrameDraw>0)
		{
			drawBackground();	
			drawScreen();	
			FrameDraw=0;
		}
		else 
		{
			FrameDraw++;
		}
		
		if (!gamepaused && myWinner==-1 && LastAiTime<GameTime)
		{
			LastAiTime=GameTime+AiUpdateTime;
			gameplayers[1-myPlayer].solveAiPlayer();
		}
		
			
			if (gameplayers[myPlayer].SelectedEntities.length==1)
			{
				MoveCamera(gameplayers[myPlayer].SelectedEntities[0].GetAttributes().x,gameplayers[myPlayer].SelectedEntities[0].GetAttributes().y);
			}
			
		if (getGamePaused())		
		{
			GameSpeed=0;
		}
		else if (GameSpeed<1)
		{
			GameSpeed=1;
		}
		GameTick++;	
		if (GameSpeed>0)
		{
			GameTime+=GameSpeed;
			
			for (var iObj =0; iObj<gameobjects.length; iObj++)//Update all game objects
			{
				var oObj = gameobjects[iObj];
				if (oObj.Update!=null)
				{oObj.Update()}
				if (oObj.AmIUnused!=undefined && oObj.AmIUnused()==true)
				{
					gameobjects.splice(iObj,1);
					iObj--;
				}
			}
		}
		//Mouse and Mouse Box
		if (MouseData.mouseDragBox)
		{
		////////console.log(MouseData);
			game.canvas.fillStyle = 'rgba(0,0,0, 0)';
			game.canvas.beginPath();
			game.canvas.rect(
				MouseData.mouseCenterX-game.DrawRect.xMin,
				MouseData.mouseCenterY-game.DrawRect.yMin,
				MouseData.mouseBoxX-MouseData.mouseCenterX,
				MouseData.mouseBoxY-MouseData.mouseCenterY
			);
			game.canvas.closePath();
			game.canvas.fill();
							
			game.canvas.lineWidth = 1;
			game.canvas.strokeStyle = 'rgba(0,0,0, 1)';
			game.canvas.stroke();
		}		
		drawUserInterface();
		//Do Power Overlay 
		if (PowerOverlay.enabled==true)
		{
			drawSpellOverlay();
		}
		else if (MouseData.lastClickTime>0 && MouseData.lastClickTime+game.FPS/5<= GameTick && Math.abs(MouseData.mouseCenterX-MouseData.mouseBoxX)<10 && Math.abs(MouseData.mouseCenterY-MouseData.mouseBoxY)<10)
		{			
			//////console.log("Drawing Power Overlay");
			MouseData.mouseDragBox=false;			
			MouseData.mousePowerUp=false;
			PowerOverlay.enabled=true;
			PowerOverlay.drawn=false;
			PowerOverlay.x=MouseData.mouseBoxX;
			PowerOverlay.y=MouseData.mouseBoxY;
			PowerOverlay.caster = undefined;
			if (gameplayers[myPlayer].SelectedEntities.length>=1)
			{
				PowerOverlay.caster = gameplayers[myPlayer].SelectedEntities[0];
			}
			PowerOverlay.target = GetMouseEntity(true);
		}
		
		gLoop = setTimeout(Update, 1000 / game.FPS);	//ensure repeat
	}
	
	var getGamePaused = function()
	{
		return gamepaused || PowerOverlay.enabled==true;
	}
	
	var getGameTime = function()
	{
		return GameTime;
	}
	
	var GetTileAt= function(iX,iY)
	{
		if (gametiles[iY]==undefined)
		{
			return null;
		}
		
		return gametiles[iY][iX];
	}
		
	var GetPlayer = function(i)
	{
		return gameplayers[i];
	}
	
	var getGameBorders = function()
	{
		return GameBorders;
	}
	
	var launchProjectileFromUnit = function(caster,type,offsetx,offsety,vx,vy,rad,weight,Damage,lifetime,collide,explosionRadius,stun,KB,pierce)
	{
		var pData = new entityProjectile (
		{
			type:type,
			x:caster.GetAbsolutePosition().x+offsetx*caster.GetAttributes().Facing,
			y:caster.GetAbsolutePosition().y+offsety,
			vx:vx*caster.GetAttributes().Facing,
			vy:vy,
			weight:weight,
			lifetime:getGameTime()+lifetime*game.FPS,
			r:rad*caster.GetSize(),
			caster:caster,
			Damage:Damage*caster.GetPower(),
			collides:collide,
			stun:stun,
			KB:KB,
			explosionRadius:explosionRadius,
			pierce:pierce,
		}
		)
		pData.setInstance(pData);
		makeNewEntity(pData);
	}
	
	var launchProjectileFromUnitToTarget = function(caster,type,offsetx,offsety,target,speed,rad,weight,Damage,lifetime,collide,explosionRadius,stun,KB,pierce)
	{
		var angle = Math.abs(Math.atan2(target.x-caster.GetAttributes().x, target.y-caster.GetAttributes().y));
		
		var weightloss = 0 - (weight*Math.abs(caster.GetAttributes().x-target.x)/speed/2+(caster.GetAttributes().y-target.y)/speed)/100;
		
		launchProjectileFromUnit(caster,type,offsetx,offsety,speed*(Math.sin (angle)),speed*(Math.cos(angle))+weightloss,rad,weight/100,Damage,lifetime,collide,explosionRadius,stun,KB,pierce);
	}
		
	var makeNewEntity = function(entity)
	{
		gameobjects.push(entity);
	}
	
	var updateScore = function(player,increase)
	{
		GetPlayer(player).Score+=increase;
		
		if (GetPlayer(player).Score>=ScoreToWin)
		{
			myWinner = player;
		}
	}
		
	return {
		initialize: initialize,
		makeNewEntity:makeNewEntity,
		Update: Update,		
		GetTileAt: GetTileAt,
		getGamePaused: getGamePaused,
		getGameTime: getGameTime,
		GameSpeed:GameSpeed,
		onKeyPress:onKeyPress,
		GetPlayer:GetPlayer,
		getGameBorders:getGameBorders,
		gameMode:gameMode,
		PickUnitsInRange:PickUnitsInRange,
		launchProjectileFromUnit:launchProjectileFromUnit,
		launchProjectileFromUnitToTarget:launchProjectileFromUnitToTarget,
		updateScore:updateScore,
		GetAllPlatforms:GetAllPlatforms,
	};
};

var entityPlatform = function(id,vPos)
{
	var ID = "Platform_"+id;
	
	var Type = vPos[5];
	var PlatformPosition = [vPos[0],vPos[1]];	
	var PlatformWidth = vPos[2];
	var PlatformHeight = vPos[3];
	var SideWall = true;		
	var CanFall = true;		
		
	if (!vPos[4])
	{
		SideWall=false;
		CanFall=false;
	}
	
	//Draw Frame by Frame
	var Draw = function()
	{
		////////console.log("Drawing Platform "+ID);
		
		/*var tileImage = new Image();
        tileImage.src = 'images/'+Type+'.png';	
		tileImage.width=90;
		tileImage.height=90;*/
		
				
		//fill			
		/*if (RectContainsRect(
		game.DrawRect,
		{
			xMin:PlatformPosition[0],
			yMin:PlatformPosition[1],
			xMax:PlatformWidth,
			yMax:PlatformHeight,
		}))*/
		{	
		
		
		var tileImage =  document.getElementById(Type);
		var DrawX=PlatformPosition[0]-game.DrawRect.xMin;
		var DrawY=PlatformPosition[1]-game.DrawRect.yMin;		
		var tileSize = 10;	
			{
				game.canvas.drawImage(tileImage, 
				1*tileSize, 1*tileSize, //x,y
				tileSize, tileSize, //w,h
				DrawX+tileSize, DrawY+tileSize,
				PlatformWidth-tileSize*2, PlatformHeight-tileSize*2);
			}
			
			//edges			
			{
				game.canvas.drawImage(tileImage, 
				0*tileSize, 0*tileSize, //x,y
				tileSize, tileSize, //w,h
				DrawX, DrawY,
				tileSize, tileSize);		
			}
				
			{
				game.canvas.drawImage(tileImage, 
				2*tileSize, 0*tileSize, //x,y
				tileSize, tileSize, //w,h
				DrawX+PlatformWidth-tileSize, DrawY,
				tileSize, tileSize);
			}
			{
				game.canvas.drawImage(tileImage, 
				0*tileSize, 2*tileSize, //x,y
				tileSize, tileSize, //w,h
				DrawX, DrawY+PlatformHeight-tileSize,
				tileSize, tileSize);			
			}
			{
				game.canvas.drawImage(tileImage, 
				2*tileSize, 2*tileSize, //x,y
				tileSize, tileSize, //w,h
				DrawX+PlatformWidth-tileSize, DrawY+PlatformHeight-tileSize,
				tileSize, tileSize);
			}
			//borders					
			{//top
				game.canvas.drawImage(tileImage, 
				1*tileSize, 0*tileSize, //x,y
				tileSize, tileSize, //w,h
				DrawX+tileSize, DrawY,
				PlatformWidth-tileSize*2, tileSize);	
			}
			{//bottom
				game.canvas.drawImage(tileImage, 
				1*tileSize, 2*tileSize, //x,y
				tileSize, tileSize, //w,h
				DrawX+tileSize, DrawY+PlatformHeight-tileSize,
				PlatformWidth-tileSize*2, tileSize);		
			}
			{		//left
				game.canvas.drawImage(tileImage, 
				0*tileSize, 1*tileSize, //x,y
				tileSize, tileSize, //w,h
				DrawX, DrawY+tileSize,
				tileSize, PlatformHeight-tileSize*2);	
			}
			{//right
				game.canvas.drawImage(tileImage, 
				2*tileSize, 1*tileSize, //x,y
				tileSize, tileSize, //w,h
				DrawX+PlatformWidth-tileSize, DrawY+tileSize,
				tileSize, PlatformHeight-tileSize*2);
			}
		}
	}
	
	var AmIinTile = function(iX,iY)
	{
		var r1 = {
			xMin:iX*game.tileSize,
			yMin:iY*game.tileSize,
			xMax:(iX+1)*game.tileSize,
			yMax:(iY+1)*game.tileSize,
		}	
		var r2 = {
			xMin:PlatformPosition[0],
			yMin:PlatformPosition[1],
			xMax:PlatformPosition[0]+PlatformWidth,
			yMax:PlatformPosition[1]+PlatformHeight,
		}		
		
		//////console.log(GetID()+' '+iX+','+iY);
		//////console.log(RectContainsRect(r1,r2));
				
		return RectContainsRect(r1,r2);
	}
	
	var AmIonScreen = function()
	{
		return true;
	}
	
	var GetType = function()
	{
		return Type_GamePlatform;
	}
	
	var GetID = function()
	{
		return ID;
	}
	
	var GetPlatformData = function()
	{
		return {x:PlatformPosition[0],y:PlatformPosition[1],w:PlatformWidth,h:PlatformHeight,SideWall:SideWall};
	}
	
	return {
		Draw:Draw,
		GetType:GetType,
		GetID:GetID,
		AmIinTile:AmIinTile,
		AmIonScreen:AmIonScreen,
		GetPlatformData:GetPlatformData,
	}
}

var entityTile = function()
{	
	var tileEntities = [];
	
	var Draw = function()
	{
		//Go through each entity/platform on this tile
		//Saves time than having to go through the entire map
		
		for (var iObj =0; iObj<tileEntities.length; iObj++)
		{
			var oObj = tileEntities[iObj];
			if (oObj.Draw!=undefined)
			{oObj.Draw()}	
		}
	}
	
	return {
		Draw:Draw,
		tileEntities:tileEntities,
	}
};

var entityBase = function()
{
	//these are the mandatory functions for every active entity	
	return {
		Draw:Draw,
		Update:Update,
		GetType:GetType,
		GetID:GetID,
		AmIonScreen:AmIonScreen,
		AmIPlayerControlled:AmIPlayerControlled,
		AmIUnused:AmIUnused,
	}
};

var entityUnit = function(iID,Pos,Side,Type,dSummoned){
	
	var entityAttributes = {
		x:Pos[0],
		y:Pos[1],
		summoned:dSummoned,
		vx:0,
		vy:0,
		width:entityData[Type].width,
		height:entityData[Type].height,
		Side:Side,
		AiLevel:entityData[Type].AiLevel,
		Description:entityData[Type].Description,
		Life:100,
		Stamina:100,
		StaminaRegen:entityData[Type].StaminaRegen,
		Armor:entityData[Type].Armor,				
		Mesh:entityData[Type].Mesh,			
		Weight:entityData[Type].Weight,
		Facing:1,			
		FallSpeed:entityData[Type].FallSpeed,
		MoveSpeed:entityData[Type].MoveSpeed,
		hitboxWidth:entityData[Type].hitboxWidth,
		JumpRange:entityData[Type].JumpRange,
		Spells:entityData[Type].Spells,
		animationData:entityData[Type].animationData,
		lastTouchedPlayer:-1,
		myPlatform:null,
	};
	
	if (!dSummoned)
	{
		entityAttributes.Life=0;
		entityAttributes.nextRespawnTime=0;
	}
	
	var instance=null;
	
	var setInstance = function(unit)
	{
		instance=unit;
	}
	
	var ID = 'Unit '+iID;
	var eData = entityData[Type];
	var myOrders = [];
	var Modifiers = [];
	
	var MoveData=[false,false,null];//x,y,my platform entity
	var Active = false;
	var Flying = false;
	var Grounded = false;
	var Jumping = false;
			
	var AnimationData =
	{
		animation:'stand',
		animationFrame:0,
		
		currentFrame:0,
		dataFrame:[0,0],
		speedFrame:0,
		powerID:-1,
		powerData:[],
		repeat:false,
		forced:false,		
	};
		
	var DealDamage = function(target,damage)
	{
		if (target!=null && target!=undefined && !target.AmIDead() && !target.AmIInvulnerable())
		{
			var eTarget = target.GetAttributes();
			damage*=GetPower();
			
			if (eTarget.Armor>damage)
			{
				damage=damage/2;
			}
			else
			{
				damage-=eTarget.Armor;
			}
			
			eTarget.Life-=damage;
			eTarget.lastTouchedPlayer = entityAttributes.Side;
			
			game.runningGame.GetPlayer(entityAttributes.Side).setUltimate(damage*UltimateChargeRate,false);
						
			if (eTarget.Life<=0)
			{
				target.doDie();
			}
				PlaySound('hurt.wav')
		}
	}
	
	var AmIInvulnerable = function()
	{
		return HasModifier('immune');
	}
	
	var DealHealing = function(target,healing)
	{
		if (target!=null && target!=undefined && !target.AmIDead())
		{
			var eTarget = target.GetAttributes();
			eTarget.Life=Math.min(eTarget.Life+healing,100);
		}
	}
	
	var KnockBack= function(target,x,y)
	{
		if (target!=null && target!=undefined)
		{
			var eTarget = target.GetAttributes();
			
			var iDir = 1;
			
			if (entityAttributes.x-eTarget.x>0)
			{
				iDir=-1;
			}
			
			eTarget.vx=x*iDir;
			eTarget.vy=0-y;
			eTarget.lastTouchedPlayer = entityAttributes.Side;
			
			target.AddModifier('dazed',eTarget.vy/eTarget.Weight);
		}
	}
	
	var CanAct = function()
	{		
		return (
			!HasModifier('dazed') && 			
			!AmIDead() && 
			(AnimationData.forced==false || AnimationData.animation=='none')			
		);
	}
	
	var CanCast = function()
	{
		return !HasModifier('inactive') && CanAct();
	}
		
	var doDie = function()
	{		
	
		if (entityAttributes.summoned)
		{
			game.runningGame.gameobjects.splice(game.runningGame.gameobjects.indexOf(instance),1);
		}
		else
		{
			AddModifier('KO',respawnTime);		
			
			entityAttributes.vx=Math.random()*entityAttributes.MoveSpeed*2-entityAttributes.MoveSpeed;
			entityAttributes.vy=0-Math.random()*entityAttributes.FallSpeed;
			
			if (entityAttributes.lastTouchedPlayer>=0 && entityAttributes.lastTouchedPlayer!=entityAttributes.Side){
				if (game.runningGame.gameMode=="deathmatch")
				{
					game.runningGame.updateScore(entityAttributes.lastTouchedPlayer,1);
				}
			}
		}
	}
	
	var doRespawn = function()
	{
		var myOwner = game.runningGame.GetPlayer(entityAttributes.Side);
		
		if (myOwner!=null) {
			
			var spawnData = -1;
			var Spawner = -1;
			
			for (var iSpawn = 0; iSpawn<myOwner.spawners.length; iSpawn++)
			{
				if (myOwner.spawners[iSpawn].lastUseTime<spawnData || Spawner==-1)
				{
					spawnData=myOwner.spawners[iSpawn].lastUseTime;
					Spawner=iSpawn;
				}
			}
			
			if (Spawner!=null)
			{
				entityAttributes.Life=100;
				entityAttributes.Stamina=100;
				entityAttributes.x=myOwner.spawners[Spawner].x;
				entityAttributes.y=myOwner.spawners[Spawner].y;
				entityAttributes.vx=0;
				entityAttributes.vy=0;
				myOrders=[];
				AddModifier('immune',2);
				
				myOwner.spawners[Spawner].lastUseTime=game.runningGame.getGameTime();
				entityAttributes.lastTouchedPlayer=-1;
				
				AnimationData.animation='none';
				PlayDefaultAnimation('spawn',false);
				AnimationData.speedFrame=1/10;
				
				PlaySound('respawn.ogg')
			}
		}
	}
	
	var AmIDead = function()
	{
		return HasModifier('KO');
	}
	
	var GetRespawnTime = function()
	{
		if (GetModifier('KO')==null)
		{
			return 0;
		}
		
		return Math.ceil((GetModifier('KO').duration-game.runningGame.getGameTime())/game.FPS);
	}
	
	var PlayAnimation = function(aData)
	{		
		if (
			aData.forced==true ||
			CanAct()
		)
		{		
			AnimationData.animation=aData.name;
			AnimationData.currentFrame=0;
			AnimationData.dataFrame=aData.dataFrame;
			AnimationData.speedFrame=aData.speedFrame;
			AnimationData.powerID=-1;
			AnimationData.forced=false;
			
			if (aData.forced==true)
			{
				AnimationData.forced=true;
			}
			if (aData.repeat==true)
			{
				AnimationData.repeat=true;
			}
			
			if (aData.powerID>=0)
			{
				AnimationData.powerID=aData.powerID;
				AnimationData.powerData=[];
				AnimationData.forced=true;
				AnimationData.repeat=false;
			}
		}
	}
	
	var PlayDefaultAnimation = function(nAnim,forced)
	{		
		var iAnim = AnimationNames.indexOf(nAnim);
		
		var aData = {
			name:AnimationNames[iAnim],			
			dataFrame:entityAttributes.animationData[iAnim],
			speedFrame:1/entityAttributes.animationData[iAnim].length,
			forced:forced,
		}
		PlayAnimation(aData);
	}
	
	var Animate = function()
	{
		if (AnimationData.animation!='none')
		{
			AnimationData.currentFrame=AnimationData.currentFrame+AnimationData.speedFrame*GetSpeed();			
						
			if (AnimationData.currentFrame>=AnimationData.dataFrame.length)
			{
				if (AnimationData.repeat)
				{
					AnimationData.currentFrame=0;
				}
				else
				{
					AnimationData.animation='none';
				}
			}
			
			if (AnimationData.powerID>=0)
			{				
				var PowerData = entityAttributes.Spells[AnimationData.powerID];
					
				for (var iPow = 0; iPow<PowerData.spellData.length; iPow++)
				{
					if (AnimationData.powerData.indexOf(iPow)==-1)
					{
						var pData = PowerData.spellData[iPow];						
						if (pData.noTarget==true)
						{
							if (pData.spell!=undefined && pData.frame<AnimationData.currentFrame)
							{
								//Cast Power														
								pData.spell({
									caster:instance,
								});
								
								AnimationData.powerData.push(iPow);
							}
						}
						else if (myOrders.length>0)
						{
							if (pData.spell!=undefined && pData.frame<AnimationData.currentFrame)
							{
								//Cast Power														
								pData.spell({
									caster:instance,
									x:myOrders[0].x,
									y:myOrders[0].y,
									target:myOrders[0].target,
								});
								
								AnimationData.powerData.push(iPow);
							}
						}					
					}
				}
			}
			
			AnimationData.animationFrame=AnimationData.dataFrame[Math.floor(AnimationData.currentFrame)];
		}
	}
			
	var DrawHitBox=false;
			
	var Draw = function()
	{
		//Draw Hitbox
		if (DrawHitBox)
		{
			game.canvas.fillStyle = 'rgba(255,0,0, 0.4)';	

			if (AmIPlayerControlled())
			{
				game.canvas.fillStyle = 'rgba(0,255,0, 0.4)';	
			}
			
			game.canvas.beginPath();
			game.canvas.rect(
			entityAttributes.x-entityAttributes.hitboxWidth*GetSize()-game.DrawRect.xMin,
			entityAttributes.y-entityAttributes.height*GetSize()-game.DrawRect.yMin,
			entityAttributes.hitboxWidth*2*GetSize(),
			entityAttributes.height*GetSize());
			game.canvas.closePath();
			game.canvas.fill();
							
			game.canvas.lineWidth = 1;
			game.canvas.strokeStyle = 'rgba(255,0,0, 1)';
			if (AmIPlayerControlled())
			{
				game.canvas.strokeStyle = 'rgba(0,255,0, 1)';	
			}
			if (AmISelected())
			{
				game.canvas.strokeStyle = 'rgba(255,255,255, 1)';	
			}
			game.canvas.stroke();
		}
				
		//fill	
		/*if (RectContainsRect(
		game.DrawRect,
		{
			xMin:entityAttributes.x,
			yMin:entityAttributes.y,
			xMax:entityAttributes.width,
			yMax:entityAttributes.height,
		}))*/		
		{
			var tileImage =  document.getElementById(entityAttributes.Mesh);
			var DrawX=entityAttributes.x-game.DrawRect.xMin;
			var DrawY=entityAttributes.y-game.DrawRect.yMin;	
		
			var Color = 1;
			if (AmIPlayerControlled())
			{
				Color = 0;				
			}
			
			game.canvas.save();
			game.canvas.scale(entityAttributes.Facing, 1);
			
			game.canvas.drawImage(tileImage, 
			AnimationData.animationFrame*entityAttributes.width, Side*entityAttributes.height, //x,y
			entityAttributes.width, entityAttributes.height, //w,h
			DrawX*entityAttributes.Facing-entityAttributes.width/2*GetSize(), DrawY-entityAttributes.height*GetSize(),
			entityAttributes.width*GetSize(), entityAttributes.height*GetSize());
			
			game.canvas.restore();
			
			if (AmISelected())
			{
				game.canvas.drawImage(document.getElementById('overhead'),
				DrawX-entityAttributes.hitboxWidth/2*GetSize()-5, DrawY-entityAttributes.height*GetSize()-9,
				15, 9);	
			}
		}
	}
	
	var GetType = function()
	{
		return Type_GameEntity;
	}
	
	var GetTile  = function()
	{
		var iX=Math.floor(entityAttributes.x/game.tileSize);
		var iY=Math.floor(entityAttributes.y/game.tileSize);
		
		return [iX,iY];
	}
	
	var GetHitBox = function()
	{
		var rect =  {
			xMin:entityAttributes.x-entityAttributes.width/2*GetSize(),
			yMin:entityAttributes.y-entityAttributes.height*GetSize(),
			xMax:entityAttributes.x+entityAttributes.width/2*GetSize(),
			yMax:entityAttributes.y
		}
		////////console.log(rect);
		return rect;
	}
	
	
	var GetNearbyTiles = function()
	{
		var vVel = 1;
		var vPos =GetTile();
		
		if (entityAttributes.vy<0)
		{
			vVel=-1;
		}
		
		return [
		game.runningGame.GetTileAt(vPos[0],vPos[1]),
		game.runningGame.GetTileAt(vPos[0]+1,vPos[1]),
		game.runningGame.GetTileAt(vPos[0]-1,vPos[1]),
		game.runningGame.GetTileAt(vPos[0]+1,vPos[1]+vVel),
		game.runningGame.GetTileAt(vPos[0]-1,vPos[1]+vVel),
		game.runningGame.GetTileAt(vPos[0],vPos[1]+vVel)];
		
	}
	
	var Update = function()
	{
		if (Active)
		{
			HandleModifiers();
			HandleMovement();
			Animate();
			
			if (!AmIDead() && entityAttributes.Life<=0)
			{
				doRespawn();
			}
			else
			{
				HandleOrders();
			}
			
			entityAttributes.Stamina= Math.min(100,entityAttributes.Stamina+entityAttributes.StaminaRegen*game.runningGame.GameSpeed)
		}	
		else
		{
			checkActive();
		}	
	}
	
	var checkActive = function()
	{
		if (!Active && (AmIonScreen() || AmIPlayerControlled()))
		{
			//////console.log('activate');
			Active = true;
		}
	}
	
	var JumpToPosition = function(vPos)
	{
		var xTime = Math.abs(vPos.x-entityAttributes.x)/entityAttributes.MoveSpeed
		var yTime = Math.abs(vPos.y-entityAttributes.y)/(entityAttributes.FallSpeed-entityAttributes.Weight)
				
		entityAttributes.vy=0-xTime/2-yTime;
		entityAttributes.vx=(vPos.x-entityAttributes.x)/(xTime+yTime);	
	}
	
	var HandleOrders = function()
	{
		if (Grounded)
		{
			entityAttributes.vx=0;
		}
	
		if (CanAct() && Grounded)
		{			
			if (myOrders.length>0)
			{
				var eOrder = myOrders[0];
				if (eOrder.solved==true)			
				{
					myOrders.splice(0,1);	
					return;
				}
				
				if (eOrder.id=='jump' && Grounded)
				{
					/*if (eOrder.y<entityAttributes.y)
					{
						entityAttributes.vy=0-entityAttributes.JumpRange;
						PlayDefaultAnimation('jump',true);
					}
					
					eOrder.id='move';*/
					
					if (Math.sqrt((entityAttributes.x-eOrder.x)*(entityAttributes.x-eOrder.x)+(entityAttributes.y-eOrder.y)*(entityAttributes.y-eOrder.y))<entityAttributes.JumpRange*GetSize()							 )
						{		
					
						
						if (eOrder.y>entityAttributes.y)
						{
							AddModifier('dive',1);
						} 
						JumpToPosition({x:eOrder.x,y:eOrder.y});
						
							eOrder.solved=true;
						}
						else
						{
							entityAttributes.vx=Math.max(-entityAttributes.MoveSpeed,Math.min(
							entityAttributes.MoveSpeed,
							eOrder.x-entityAttributes.x));
						}
					
					
				}
				else if (eOrder.id=='move')
				{
					if (Math.abs(eOrder.x-entityAttributes.x)>entityAttributes.MoveSpeed)
					{
						if (eOrder.x>entityAttributes.x)
						{
							entityAttributes.vx=entityAttributes.MoveSpeed;
						}
						else if (eOrder.x<entityAttributes.x)
						{
							entityAttributes.vx=0-entityAttributes.MoveSpeed;
						}
					}
					else
					{
						entityAttributes.x=eOrder.x;
						entityAttributes.vx=0;
						myOrders.solved=true;
					}
				}
				else if (eOrder.id=='power')
				{
					var pPower = entityAttributes.Spells[eOrder.data];										
					if (entityAttributes.Spells[eOrder.data].targetless)
					{
						CastPower(eOrder.data);
							
							if (eOrder.repeat!=true)
							{
								eOrder.solved=true;
							}
					}
					else if (eOrder.target!=null)
					{	
						if (eOrder.target.AmIDead() || eOrder.target==instance)
						{
								eOrder.solved=true;
						}
						else if ((
							pPower.range<=entityAttributes.hitboxWidth && 			
							Math.abs(GetAbsolutePosition().x-eOrder.target.GetAbsolutePosition().x)<=entityAttributes.hitboxWidth*GetSize()+eOrder.target.GetAttributes().hitboxWidth*eOrder.target.GetSize() &&
							Math.abs(eOrder.target.GetAbsolutePosition().y-GetAbsolutePosition().y)<=eOrder.target.GetAbsolutePosition.r*eOrder.target.GetSize()+entityAttributes.hitboxWidth
							) || (							
							Math.abs(entityAttributes.x-eOrder.target.GetAttributes().x)<=pPower.range*GetSize()+eOrder.target.GetAttributes().hitboxWidth*eOrder.target.GetSize() &&
							Math.abs(entityAttributes.y-eOrder.target.GetAttributes().y)<=Math.tan(pPower.angle)*Math.abs(entityAttributes.x-eOrder.target.GetAttributes().x)
							
						))
						{	

						if (CanCast()){		

						if (eOrder.target!=null){
							if (entityAttributes.x-eOrder.target.GetAttributes().x>0)
							{
								entityAttributes.Facing=-1;
							}
							else if (entityAttributes.x-eOrder.target.GetAttributes().x<0)
							{
								entityAttributes.Facing=1;
							}							
						}
					
						
							CastPower(eOrder.data);
							
							if (eOrder.repeat!=true)
							{
								eOrder.solved=true;
							}
						}
						}
						else
						{
							entityAttributes.vx=Math.max(-entityAttributes.MoveSpeed,Math.min(
							entityAttributes.MoveSpeed,
							eOrder.target.GetAttributes().x-entityAttributes.x));
						}
					}
					else
					{						
						if ((
							pPower.range<=entityAttributes.hitboxWidth && 			
							Math.abs(entityAttributes.x-eOrder.x)<=entityAttributes.hitboxWidth*GetSize() &&
							Math.abs(eOrder.y-(entityAttributes.y-entityAttributes.height/2)<=entityAttributes.hitboxWidth/2)
							) || (
							Math.abs(entityAttributes.x-eOrder.x)<=pPower.range*GetSize() &&
							Math.abs(entityAttributes.y-eOrder.y)<=Math.tan(pPower.angle)*Math.abs(entityAttributes.x-eOrder.x)
							
						))
						{		
						if (CanCast()){
							
							
							if (entityAttributes.x-eOrder.x>0)
							{
								entityAttributes.Facing=-1;
							}
							else if (entityAttributes.x-eOrder.x<0)
							{
								entityAttributes.Facing=1;
							}							
						
							
							CastPower(eOrder.data);
							if (eOrder.repeat!=true)
							{
								eOrder.solved=true;
							}
						}
						}
						else
						{
							entityAttributes.vx=Math.max(-entityAttributes.MoveSpeed,Math.min(
							entityAttributes.MoveSpeed,
							eOrder.x-entityAttributes.x));
						}
					}
				}
			}	
			
			if (entityAttributes.vx<0)
			{
				entityAttributes.Facing=-1;
			}
			else if (entityAttributes.vx>0)
			{
				entityAttributes.Facing=1;
			}
		}
	}
	
	var CastPower = function(PowerID)
	{	
		var pPower = entityAttributes.Spells[PowerID];
		
		if (entityAttributes.Stamina>=pPower.cost)
		{
			entityAttributes.Stamina-=pPower.cost;
			
			var aAnimation = 
			{
				name:'power',
				speedFrame:pPower.animSpeed/(pPower.animData.length)/3,
				dataFrame:pPower.animData,
				powerID:PowerID,
				forced:true,
			};
		
		PlayAnimation(aAnimation);
		}
		AddModifier('inactive',1);
	}
		
	var HandleMovement = function()
	{
		MoveData=[true,true,null];
		Grounded=false;
		entityAttributes.myPlatform=null;
		
		if (!AmIDead())
		{
		var accountPlatforms = [];
		
		var testTiles = GetNearbyTiles();
		
		for (var iTile = 0; iTile<testTiles.length; iTile++)
		{
			var tempTile = testTiles[iTile];
			if (tempTile!=undefined && tempTile!=null)
			{
				for (var iPlatform = 0; iPlatform<tempTile.tileEntities.length; iPlatform++)
				{
					accountPlatforms.push(tempTile.tileEntities[iPlatform]);
				}
			}
		}
		
		////////console.log(accountPlatforms.length);		
		if (accountPlatforms.length>0){
		for (var iPlatform = 0; iPlatform<accountPlatforms.length; iPlatform++)
		{
			var tempPlatform = accountPlatforms[iPlatform];
			
			if (tempPlatform!=null){
				
				var PlData = tempPlatform.GetPlatformData();
			//Check the Sides
			if (entityAttributes.vx!=0)
			{
				if (PlData.SideWall && 
				entityAttributes.y > PlData.y && 
				entityAttributes.y < PlData.y + PlData.h +entityAttributes.height * GetSize()
				)
				{
					if (entityAttributes.vx > 0 &&
						entityAttributes.x - entityAttributes.width * GetSize() / 2 <= PlData.x &&
						entityAttributes.x + entityAttributes.width * GetSize() / 2 + GetVelX() >= PlData.x
					) {
						entityAttributes.x = PlData.x - entityAttributes.width * GetSize() / 2;
						MoveData [0] = false;
					} else if (entityAttributes.vx < 0 &&
						entityAttributes.x + entityAttributes.width * GetSize() / 2 >= PlData.x + PlData.w &&
						entityAttributes.x - entityAttributes.width * GetSize() / 2 + GetVelX() <= PlData.x + PlData.w
					) {
						entityAttributes.x = PlData.x + PlData.w + entityAttributes.width * GetSize() / 2;
						MoveData [0] = false;
					}
				}	
			}
										
			//Fall ON the platform
			if (
				entityAttributes.x + entityAttributes.width * GetSize() / 2 > PlData.x && 			
				entityAttributes.x - entityAttributes.width * GetSize() / 2 < PlData.x + PlData.w 
			) 
			{
				var diving = HasModifier('dive');
				if (
				entityAttributes.vy >= 0 &&
				(
					(PlData.SideWall == false && diving == false)
					||
					PlData.SideWall == true
				)) 
				{
					
					if (
						PlData.y >= entityAttributes.y && 
						PlData.y < entityAttributes.y + GetVelY()
						) 
						{
							entityAttributes.y = PlData.y;
							MoveData [1] = false;
							Grounded = true;
							platform = tempPlatform;
							entityAttributes.myPlatform=tempPlatform;
						}
				}
				else
				{
					if (
					PlData.SideWall == true && 
					(					
						PlData.y+PlData.h < entityAttributes.y + entityAttributes.height * GetSize()  && 
						PlData.y+PlData.h >= entityAttributes.y + entityAttributes.height * GetSize()
					))
					{
						entityAttributes.y = PlData.y + entityAttributes.height * GetSize() + PlData.h;
						MoveData [1] = false;
					}
				}
			}		
			}	
		}			
		}
		}
		
		if (MoveData [0] == false) 
		{							
			entityAttributes.vx = 0;				
		}
		
		if (MoveData [1] == false) 
		{			
			entityAttributes.vy = 0;
		}
		
		entityAttributes.x = entityAttributes.x + GetVelX();
		entityAttributes.y = entityAttributes.y + GetVelY();
		
		if (!Flying) //Gravity
		{
			if (entityAttributes.vy < entityAttributes.FallSpeed) 
			{			
				entityAttributes.vy = entityAttributes.vy + entityAttributes.Weight*GetSpeed();
			}
		}
		
		//Animations		
		if (!Grounded)
		{
			if (entityAttributes.vy<0)
			{
				PlayDefaultAnimation('jump',false);
			}
			else {
				PlayDefaultAnimation('fall',false);
			}
		}
		else {			
			if (entityAttributes.vx==0)
			{
				PlayDefaultAnimation('stand',false);
			}
			else if (AnimationData.animation!='walk')
			{
				PlayDefaultAnimation('walk',false);
			}				
		}
		
		if (entityAttributes.y>game.runningGame.getGameBorders()[1] && !AmIDead())
		{
			entityAttributes.Life=0;
			doDie();
		}
		
		
		if (HasModifier('dazed') || AmIDead())
		{
			PlayDefaultAnimation('hurt',true);
		}
	}
	
	var CastUltimate = function()
	{
		
		entityAttributes.Life=100;
		//entityAttributes.Stamina=100; - too strong
		RemoveModifier('dazed');
		entityAttributes.vx=0;
		entityAttributes.vy=0;
		PlaySound('ultimate.wav')
		
		for (var iPow = 0; iPow<entityAttributes.Spells.length; iPow++)
		{
			var MyUltimate = entityAttributes.Spells[iPow];
			
			if (MyUltimate.ultimate==true)
			{
				CastPower(iPow);
				break;
			}
		}
	}
	
	var GetVelX = function()
	{
		return entityAttributes.vx*GetSpeed();
	}
	
	var GetVelY = function()
	{
		return entityAttributes.vy*GetSpeed();
	}
	
	var AmIonScreen = function()
	{
		return true;
	}
	
	var GetModifier = function(name)
	{
		for (var iMod = 0; iMod<Modifiers.length; iMod++)
		{
			var Mod = Modifiers[iMod];
			
			if (Mod.data == name)
			{
				return Mod;
			}
		}
		return null;
	}
	
	var HasModifier = function(name)
	{
		return GetModifier(name)!=null;
	}
	
	var RemoveModifier = function(name)
	{
		for (var iMod = 0; iMod<Modifiers.length; iMod++)
		{	
			if (Modifiers[iMod].data==name)
			{
				Modifiers.splice(iMod, 1);
				iMod--;
			}
		}
	}
	
	var AddModifier = function(idata,iduration)
	{
		Modifiers.push({data:idata,duration:game.runningGame.getGameTime()+iduration*game.FPS});
	}
	
	var GetSize = function()
	{
		if (HasModifier('overpower'))
		{
			return 1.5;
		}
		
		return 1;
	}
	
	var GetPower = function()
	{
		if (HasModifier('overpower'))
		{
			return 2;
		}
		return 1;
	}
	
	var HandleModifiers = function()
	{
		for (var iMod = 0; iMod<Modifiers.length; iMod++)
		{	
			if (Modifiers[iMod].duration<=game.runningGame.getGameTime())
			{
				if (Modifiers[iMod].data=='KO')
				{
					doRespawn();
				}
				Modifiers.splice(iMod, 1);
				iMod--;
			}
		}
	}
	
	var GetAbsolutePosition = function()
	{
		return {
			x:entityAttributes.x,
			y:entityAttributes.y-entityAttributes.height * GetSize()+entityAttributes.hitboxWidth * GetSize(),
			r:entityAttributes.hitboxWidth * GetSize()
		}
	}
	
	var AmISelected = function()
	{
		return game.runningGame.GetPlayer(entityAttributes.Side).SelectedEntities.indexOf(instance)>=0;;
	}
	
	var AmIPlayerControlled = function()
	{
		return entityAttributes.Side==0;
	}
	
	var GetID = function()
	{
		return ID;
	}
		
	var GetAttributes = function()
	{
		return entityAttributes;
	}
	
	var AddOrder = function (order,clear)
	{
		if (clear)
		{
			myOrders = [];
		}
		myOrders.push(order);
	}
	
	var GetSpeed=function()
	{
		return  game.runningGame.GameSpeed;
	}
	
	var AmIUnused=function()
	{
		return  false;
	}	
	
	var getOrders = function()
	{
		return myOrders;
	}
	
	return {
		Draw:Draw,
		Update:Update,
		GetType:GetType,
		GetID:GetID,
		getOrders:getOrders,
		AmIonScreen:AmIonScreen,
		AmIPlayerControlled:AmIPlayerControlled,
		GetHitBox:GetHitBox,
		AmISelected:AmISelected,
		GetAttributes:GetAttributes,
		CanAct:CanAct,
		GetSize:GetSize,
		GetPower:GetPower,
		AddOrder:AddOrder,
		CastUltimate:CastUltimate,
		GetRespawnTime:GetRespawnTime,
		AmIDead:AmIDead,
		DealDamage:DealDamage,
		DealHealing:DealHealing,
		KnockBack:KnockBack,
		setInstance:setInstance,
		AddModifier:AddModifier,
		doDie:doDie,
		GetAbsolutePosition:GetAbsolutePosition,
		AmIValidUnit:true,
		AmIUnused:AmIUnused,
		AmIInvulnerable:AmIInvulnerable,
	}
}

var entityProjectile = function(Data){
	
	var x = Data.x;
	var y = Data.y;
	var r = Data.r;
	var vx = Data.vx;
	var vy = Data.vy;
	var lifetime = Data.lifetime;
	var weight = Data.weight;
	var Owner = Data.caster;
	var type = Data.type;
	var dead = false;
	var stun = Data.stun;
	var KB = Data.KB;
	var pierce = Data.pierce;
	var targets = [];
	
	var Damage = Data.Damage;
	
	var explosionRadius=Data.explosionRadius;
	var collides=Data.collides;
	
	//Delegates
	//var onCollide = Data.onCollide;
	//var onHitUnit = Data.onHitUnit;
	//var onDeath = Data.onDeath;
	
	var instance=null;
	
	var setInstance = function(unit)
	{
		instance=unit;
	}
	
	var DrawHitBox=false;
			
	var Draw = function()
	{
		//Draw Hitbox
		if (DrawHitBox)
		{
			game.canvas.fillStyle = 'rgba(255,0,0, 0.4)';	

			if (Owner.GetAttributes().Side == game.runningGame.myPlayer)
			{
				game.canvas.fillStyle = 'rgba(0,255,0, 0.4)';	
			}
			
			game.canvas.beginPath();
			game.canvas.rect(
			x-r/2-game.DrawRect.xMin,
			y-r/2-game.DrawRect.yMin,
			r,r);
			game.canvas.closePath();
			game.canvas.fill();
						
			
		}
				
		//fill	
		/*if (RectContainsRect(
		game.DrawRect,
		{
			xMin:entityAttributes.x,
			yMin:entityAttributes.y,
			xMax:entityAttributes.width,
			yMax:entityAttributes.height,
		}))	*/	
		{
			var tileImage =  document.getElementById(type);
			var DrawX=x-game.DrawRect.xMin;
			var DrawY=y-game.DrawRect.yMin;	
								
			game.canvas.save();
			
			game.canvas.translate(DrawX,DrawY);
			
			game.canvas.rotate(Math.atan2(0-vx,vy));
			
			game.canvas.drawImage(tileImage,0-r/2,0-r/2,r,r);
			
			game.canvas.restore();
		}
	}
	
	var Update = function()
	{
		//if (onCollide!=undefined || onCollide!=null)
		if (collides)
		{
			var accountPlatforms = [];
			
			
				var tempTile = game.runningGame.GetTileAt(Math.floor(x/game.tileSize),Math.floor(y/game.tileSize));
				if (tempTile!=undefined && tempTile!=null)
				{
					for (var iPlatform = 0; iPlatform<tempTile.tileEntities.length; iPlatform++)
					{
						accountPlatforms.push(tempTile.tileEntities[iPlatform]);
					}
				}
			
			if (accountPlatforms.length>0){
				for (var iPlatform = 0; iPlatform<accountPlatforms.length; iPlatform++)
				{
					var tempPlatform = accountPlatforms[iPlatform];
					
					if (tempPlatform!=null)
					{
						
					var PlData = tempPlatform.GetPlatformData();
						if (tempPlatform.SideWall && RectContainsPoint({x:x,y:y},{xMin:PlData.x,yMin:PlData.y,xMax:PlData.x+PlData.width,yMax:PlData.y+PlData.height}))
						{
							//onCollide(instance);
							doDie();
						}
					}
				}
			}
		}
		
		//if (onHitUnit!=undefined || onHitUnit!=null)
		{
		var nEntities = game.runningGame.PickUnitsInRange(x,y,r,1-Owner.GetAttributes().Side);
		
		if (nEntities.length>0)
		{
			//onHitUnit(instance,nEntities);
			if (explosionRadius==0)
			{
				HurtUnits (nEntities);

				if (!pierce)
				{
					doDie();
				}
			}
			else 
			{
				doDie();
			}
			
		}
		}
		
		x+=vx;
		y+=vy;
		if (weight!=0){
		vy=Math.min(10,vy+weight);
		}
		
		if (game.runningGame.getGameTime()>=lifetime)
		{
			doDie();
		}
	}
	
	var doDie = function()
	{
		//if (onDeath!=undefined || onDeath!=null)
			if (explosionRadius>0)
		{
		onDeath(instance);
		}
		//if (entityGame.gameobjects.indexOf(instance)!=-1)
		//{entityGame.gameobjects.splice(entityGame.gameobjects.indexOf(instance),1);}
		dead=true;
	}

	var onDeath = function(instance)
	{
		var nEntities = game.runningGame.PickUnitsInRange(x,y,explosionRadius,1-Owner.GetAttributes().Side);
		
		if (nEntities.length>0)
		{
			HurtUnits (nEntities);
		}
		game.runningGame.makeNewEntity(new explosion(x,y,explosionRadius));
	}
	
	var HurtUnits = function(nUnits)
	{
		for (var eEnt = 0; eEnt<nUnits.length; eEnt++)
		{
			if (!pierce || targets.indexOf(nUnits[eEnt].GetID())==-1)
			{
				targets.push(nUnits[eEnt].GetID());
				Owner.DealDamage(nUnits[eEnt],Damage);
				if (stun>0)
				{
					nUnits[eEnt].AddModifier('dazed',stun)	
					Owner.KnockBack(nUnits[eEnt],KB,KB)
				}
			}
		}
	}
	
	var GetType = function()
	{
		return Type_Misc;
	}
	
	var AmIUnused = function()
	{
		return dead;
	}
	
	return {
		setInstance:setInstance,
		Draw:Draw,
		Update:Update,
		doDie:doDie,
		GetType:GetType,
		AmIUnused:AmIUnused,
	}
}

var StartNewGame = function()
{
	game.runningGame = new entityGame();
		game.runningGame.initialize(GetLevel());

			window.addEventListener("keydown", function(event)
			{
					game.runningGame.onKeyPress(event);
			},false
			)  
}

//on load
window.addEventListener("load", function() {
	
    if (!!document.createElement("canvas").getContext) {
        
		StartNewGame();
		      
		
    } else {
		
        alert("Support for canvas not detected!");
		
    }
	
},false
)

//Sounds
var BGM = new Audio('sounds/song_game.ogg');
BGM.play();
BGM.muted=false;
BGM.onended = function() {
    BGM.play();
};

var PlaySound = function(name)
{
	console.log(name);
if (!BGM.muted){
	var audio = new Audio('sounds/'+name);
	audio.play();}	
}

var explosion = function (lx,ly,r)
{
	var Sprite = [60,80];		
	var MaxSprite = 15;
	var dead = false;
	
	var Frame = 0;
	var x=lx;
	var y=ly;	
	
	PlaySound('bomb.wav')
	
	var Update = function()	//entity update
	{				
		if (!game.runningGame.getGamePaused())//controlls
		{						
			Frame+=game.runningGame.GameSpeed;
			if (Frame>=MaxSprite)
			{dead = true;}
		}		
	}
	
	var Draw = function()	//entity draw
	{			
		game.canvas.drawImage(document.getElementById("explosion"), Math.floor(Frame)*Sprite[0], 0, Sprite[0], Sprite[1], x-r-game.DrawRect.xMin, y-r-game.DrawRect.yMin, r*2, r*2);
	}
	
	var GetType = function()
	{
		return Type_Misc;
	}
	
	var AmIUnused = function()
	{
		return dead;
	}
	
	return {
		Draw: Draw,
		Update: Update,
		GetType:GetType,
		AmIUnused:AmIUnused,
	};
}

 game.toggleMusicBtn.addEventListener("click",	//mute button
            function() {
                BGM.muted=!BGM.muted;
            });